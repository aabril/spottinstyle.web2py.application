# coding: utf8
# try something like
import json

def index():
    photo_tags = db(db.photo_tags.styles==request.vars.style).select()

    photos = []
    for tag in photo_tags:
        photo = {}
        photo['id'] = tag.photos.id
        photo['filename'] = tag.photos.filename
        photo['owner'] = tag.photos.owner_id.username
        photos.append(photo)

    photos = json.dumps(photos)

    return photos

def tags():
    photoid = request.vars.id
    tags = db(db.photo_tags.photos==photoid).select(db.photo_tags.ALL)

    tagsformatted = []

    for tag in tags:
        tagformatted = {}
        tagformatted['id'] = tag.styles.id
        tagformatted['owner'] = tag.styles.owner_id.username
        tagformatted['name'] = tag.styles.name
        tagsformatted.append(tagformatted)

    tagsformatted = json.dumps(tagsformatted)
    return tagsformatted

def gallerytagsphoto():
    photoid = request.vars.photoid
    photoid = hashid.decrypt(photoid)[0]
    tags = db(db.photo_tags.photos==photoid).select(db.photo_tags.ALL)

    tagsformatted = []

    for tag in tags:
        tagformatted = {}
        tagformatted['id'] = hashid.encrypt(tag.styles.id)
        tagformatted['owner'] = IS_SLUG.urlify(tag.styles.owner_id.username)
        tagformatted['name'] = tag.styles.name
        tagformatted['totalloves'] = db(db.loves.style_id==tag.styles.id).count()
        tagsformatted.append(tagformatted)

    #print tagsformatted
    #blabla = sorted(tagsformatted, key = lambda tag: tag['nloves'])
    #print blabla

    tagsformatted = json.dumps(tagsformatted)
    return tagsformatted

def posttagtophoto():
    output = {}

    photoid = request.vars.photoid
    styleid = request.vars.styleid

    photoid = hashid.decrypt(photoid)[0]
    styleid = hashid.decrypt(styleid)[0]

    print "bien"
    print photoid
    print styleid

    db.photo_tags.insert(author=auth.user, photos=photoid, styles=styleid)

    #return styleid info for add the tag dinamically
    style = db(db.styles.id==styleid).select().first()
    output['owner'] = style.owner_id.username
    output['ownerlink'] = IS_SLUG.urlify(style.owner_id.username)
    output['name']  = style.name
    output['loves'] = 0

    return response.json(output)

def postnewtag():
    output = {}

    photoid = request.vars.photoid
    stylename = request.vars.stylename

    #stylename = IS_SLUG.urlify(stylename)

    photoid = hashid.decrypt(photoid)[0]

    # check if style exists
    style = db((db.styles.name==stylename) & (db.styles.owner_id==auth.user)).count()
    if style>0:
       print "this style already exists for this user"
       output["error"] = "01"
    else:
       styleid = db.styles.insert(owner_id=auth.user, name=stylename)

    output["stylelink"] = "/"+IS_SLUG.urlify(auth.user.username)+"/"+IS_SLUG.urlify(stylename)

    # ToDo: check if photo_tags already exists
    db.photo_tags.insert(author=auth.user, photos=photoid, styles=styleid)

    return response.json(output)

@auth.requires_login()
def followstyle():
    output = {}
    styleid = request.vars.style_id
    styleid = hashid.decrypt(styleid)[0]
    db.following_styles.insert(follower=auth.user_id, follows=styleid)
    return response.json(output)

@auth.requires_login()
def unfollowstyle():
    output = {}
    styleid = request.vars.style_id
    styleid = hashid.decrypt(styleid)[0]
    db((db.following_styles.follower==auth.user_id) & (db.following_styles.follows==styleid)).delete()
    return response.json(output)

@auth.requires_login()
def lovephoto():
    output = {}
    photoid = request.vars.photoid
    styleid = request.vars.styleid

    photoid = hashid.decrypt(photoid)[0]
    styleid = hashid.decrypt(styleid)[0]

    #ToDo: If already exists remove, if not add.
    if db( (db.loves.photo_id==photoid) & (db.loves.style_id==styleid) & (db.loves.owner_id==auth.user_id) ).count()==0:
       db.loves.insert(photo_id=photoid, style_id=styleid)
    else:
       db( (db.loves.photo_id==photoid) & (db.loves.style_id==styleid) & (db.loves.owner_id==auth.user_id) ).delete()


    return response.json(output)


@auth.requires_login()
def loves():
    photoid = request.vars.photoid
    loves = db(db.loves.photo_id==photoid).select().json()
    return loves

def photos():
    photos = db().select(db.photos.ALL, limitby=(0,9))
    return dict(photos=photos)

def userexists():
    user    = request.vars.user
    query   = db.auth_user.username.like(user)

    if db(query).count() >= 1:
        exists = 1
    else:
        exists = 0

    return exists

@auth.requires_login()
def uploadphoto():
    output = {}
    userid = auth.user.id

    try:
        photo = request.vars.file
        photoid = db.photos.insert(owner_id=userid, filename=db.photos.filename.store(photo.file, photo.filename))
        output['photoid'] = hashid.encrypt(photoid)
    except:
       output['error'] = 1
       output[''] = "Can't read photo"

    response.headers['Content-Type']='application/json'
    return response.json(output)

def search():
    output = {}
    search = request.vars.search
    output['users']  = {}
    output['styles'] = {}

    users  = db(db.auth_user.username.contains(search)).select( limitby=(0,4) )
    styles = db(db.styles.name.contains(search)).select( limitby=(0, 8) )

    # users json
    for i in range(len(users)):
        followers = db(db.following_users.follows==users[i].id).count()
        following = db(db.following_users.follower==users[i].id).count()

        output['users'][i] = {}
        output['users'][i]['username']  = users[i].username
        output['users'][i]['followers'] = followers
        output['users'][i]['following'] = following

    # styles json
    for i in range(len(styles)):
        sumloves = 0
        photos = db(db.photo_tags.styles==styles[i].id).select()
        for photo in photos:
            photo.loves = db(db.loves.photo_id==photo.photos).count()
            sumloves = sumloves + photo.loves

        # avatar
        try:
            photoid = photos.last().photos # ToDo: select the most loved photo
            thumb   = db(db.photos.id==photoid).select(db.photos.thumb).first().thumb
        except:
            thumb = False

        output['styles'][i] = {}
        output['styles'][i]['name']  = styles[i].name
        output['styles'][i]['owner'] = styles[i].owner_id.username
        output['styles'][i]['loves'] = sumloves
        output['styles'][i]['thumb'] = thumb

    response.headers['Content-Type'] = 'application/json'
    return response.json(output)

def getphototags():
    output = {}
    photoidhash = request.args[0]
    photoid = int(hashid.decrypt(photoidhash)[0])

    output[str(photoidhash)] = {}

    phototags = db(db.photo_tags.photos==photoid).select()
    for phototag in phototags:
        output[str(photoidhash)]['name'] = phototag.styles.name
        output[str(photoidhash)]['owner'] = phototag.styles.owner_id.username

    return response.json(output)

def hashtoid():
    output = {}
    if request.args[0]:
       output["id"] = hashid.decrypt(request.args[0])
    return response.json(output)

def updatestyledescription():
    output = {}
    style_id = request.vars.style_id
    style_description = request.vars.style_description

    style_id = hashid.decrypt(style_id)[0]

    style = db(db.styles.id==style_id).select().first()
    style.description = style_description
    style.update_record()

    return response.json(output)



def searchstyles():
    output = {}
    search = request.vars.searchReq
    styleresults = db(db.styles.name.contains(search)).select( limitby=(0,16) )

    for i in range(len(styleresults)):
        output[i] = {}
        output[i]['name']              = styleresults[i].name
        output[i]['owner']             = styleresults[i].owner_id.username
        output[i]['ownerlink']         = IS_SLUG.urlify(styleresults[i].owner_id.username)
        output[i]['stylelink']         = "/"+IS_SLUG.urlify(styleresults[i].owner_id.username)+"/"+output[i]['name']
        output[i]['styleid']           = hashid.encrypt(styleresults[i].id)

        if styleresults[i].description=="" or styleresults[i].description==None:
           output[i]["styledescription"] = "No description"
        else:
            if len(styleresults[i].description)>16:
                output[i]["styledescription"]  = styleresults[i].description[0:16]+"..."
            else:
                output[i]["styledescription"]  = styleresults[i].description

        output[i]["stylethumb"]        = URL('default', 'download', args=styleresults[i].stylethumb)
        output[i]['loves']             = db(db.loves.style_id==styleresults[i].id).count()

    return response.json(output)

def uploadtagstyle():
    output = {}

    uploadtagstyle = request.vars.uploadtagstyle
    uploadtagstyle = json.loads(uploadtagstyle)

    styletag = hashid.decrypt(uploadtagstyle['styletag'])[0]

    photoids = json.loads( uploadtagstyle['photoids'] )

    for photoid in photoids:
        photo = db(db.photos.id==hashid.decrypt(photoid)[0]).select().first()
        style = db(db.styles.id==styletag).select().first()
        db.photo_tags.insert(photos=photo, styles=style, author=auth.user)

    return response.json(output)

def uploadnewtag():
    output = {}

    uploadtagstyle = request.vars.uploadtagstyle
    uploadtagstyle = json.loads(uploadtagstyle)
    newstyle = request.vars.newstyle

    photoids = json.loads( uploadtagstyle['photoids'] )

    style = db.styles.insert(name=newstyle)

    for photoid in photoids:
        photo = db(db.photos.id==hashid.decrypt(photoid)[0]).select().first()
        db.photo_tags.insert(photos=photo, styles=style, author=auth.user)

    userlink = IS_SLUG.urlify(auth.user.username)

    output["stylelink"] = "/"+userlink+"/"+newstyle

    return response.json(output)

def updateprofileinfo():
    output = {}

    firstname = request.vars.firstname
    lastname  = request.vars.lastname
    email     = request.vars.email

    print "updateprofileinfo -----"
    print "% : "+str(email)
    print "% : "+str(firstname)
    print "% : "+str(lastname)

    return response.json(output)




#################
# dev helpers
#################

#  def changeowner():
#      output = {}
#      results = db(db.styles.owner_id==1).select()
#      print results
#      for result in results:
#          result.owner_id=17
#          result.update_record()
#      return response.json(output)

# def styledescription():
#     output = {}
#     styles = db().select(db.styles.ALL)
#
#     for style in styles:
#         if style.description==None:
#             print style.id
#             print style.name
#             style.description = ""
#             style.update_record()
#     return response.json(output)

# def makeslugs():
#    output = {}
#
#    users = db().select(db.auth_user.ALL)
#    for user in users:
#       if not user.slug:
#             user.slug = IS_SLUG.urlify(user.username)
#             user.update_record()
#             output[user.username] = "ok"
#
#    return response.json(output)

# def checkphoto():
#    output = {}
#    lasphoto = db().select(db.photos.ALL).last()
#    print lastphoto
#    return response.json(output)

# def insertbio():
#     output = {}
#     albert = db(db.auth_user.id==15).select().first()
#     albert.bio = "Developing with style."
#     albert.update_record()
#
#     return response.json(output)

# def updatethumbs():
#     output = {}
#     try:
#         print request.args[0]
#         print request.args[1]
#
#         fromphoto = int(request.args[0])
#         tophoto   = int(request.args[1])
#
#         photos = db( (db.photos.id>fromphoto) & (db.photos.id<tophoto) ).select()
#
#         for photo in photos:
#             photo.thumb = IMGTHUMB(photo.filename)
#             photo.update_record()
#
#         output["error"] = "ok"
#         print "ok"
#     except:
#         output["error"] = "ajax/updatethumbs : no arguments given"
#         print "ajax/updatethumbs : no arguments given"
#
#     return response.json(output)
#
def updatestylethumbs():
    output = {}
    styles = db(db.styles.id>0).select()

    for style in styles:
        photo = db(db.photo_tags.styles==style.id).select().first()
        if photo is not None:
            if style.stylethumb is None:
               print photo.photos.filename
               style.stylethumb =  IMGTHUMB( photo.photos.filename, 300, 110, 110)
               style.update_record()

    return response.json(output)


#def fixfarringdon():
#    output = {}
#    photo = db(db.photos.id==694).select().first()
#    photo.height = 3264
#    photo.update_record()
#    return response.json(output)

# def removestyleswithnophotos():
#     output = {}
#     styles = db(db.styles.id>0).select()
#     for style in styles:
#         if db(db.photo_tags.styles==style).count()==0:
#             #style.delete()
#             del db.styles[style.id]
#             print style
#     return response.json(output)
#

#def updatefrommobile():
#    output = {}
#    photos = db(db.photos.id>399).select()
#    for photo in photos:
#        photo.frommobile = False
#        photo.update_record()
#    return response.json(output)

#def updatemobilesize():
#    output = {}
#    photos = db(db.photos.id>399).select()
#    for photo in photos:
#
#        #if photo.height==3264:
#        #    photo.height=933
#        #if photo.width==2448:
#        #    photo.width==700
#
#        if photo.height==933:
#            photo.height=733
#        if photo.width==700:
#            photo.width==580
#
#        photo.update_record()
#    return response.json(output)


def looksloadproducts():
    output = {}
    search = request.vars.searchReq

    if search is not "":
       import requests
       shopstyleurl = "http://api.shopstyle.com/api/v2/products?pid="
       shopstylekey = "uid9456-24085616-59"
       r = requests.get(shopstyleurl+shopstylekey+"&fts="+search+"&offset=0&limit=5")
       output = r.json()

    return response.json(output)


def lookscreatelook():
    output = {}
    content = request.vars.htmlcontent
    photoid = hashid.decrypt(request.vars.photoid)[0]
    db.looks.insert(htmlcontent=content, photo_id=photoid)
    return response.json(output)


def mostloved():
    output = {}
    #mostloved = db().select(db.loves.ALL, groupby=db.loves.style_id)
    mostloved = db().select(db.loves.style_id, orderby=db.loves.style_id, groupby=db.loves.style_id)
    print mostloved

    lovesbystyle = {}
    for i in mostloved:
        print db(db.loves.style_id==i.style_id).count()
        lovesbystyle[i.style_id] = db(db.loves.style_id==i.style_id).count()

    print "----"
    print lovesbystyle
    print "----"
    lovesbystylesorted = sorted(lovesbystyle.items(), key=lambda x: x[1])
    print "----"
    print lovesbystylesorted
    print "----"
    for k,v in lovesbystylesorted:
        print k
        print v
    print "----"

    return response.json(output)





