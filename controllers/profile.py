# coding: utf8
# try something like

def index():
   urluser = request.args[0]

   if db(db.auth_user.slug==urluser).count() > 0:
       user = db(db.auth_user.slug==urluser).select().first()
       username = user.username

       bio = user.bio
       followers = 0
       following = 0
       loves = 0

       response.meta.description = "Liked a photo on "+urluser+" profile."

       styles = False
       tags = False
       looks = False
       phototags = {}

       styles = db(db.styles.owner_id==user.id).select()
       photos = db(db.photos.owner_id==user.id).select(limitby=(0,49), orderby=~db.photos.id)
       tags = db(db.photo_tags.photos==photos.first()).select()

       for tag in tags:
           tag.styles.totalloves = db(db.loves.style_id==tag.styles.id).count()

       for photo in photos:
           photo.totalloves = db(db.loves.photo_id==photo.id).count()

       if photos:
          height = SIZEDHEIGHT(photos.first().width, photos.first().height)
       else:
          height = 500
   else:
       user = phototags = photos = height = styles = tags = looks = followers = following = loves = False

   return dict(user=user, phototags=phototags, photos=photos, height=height, styles=styles, tags=tags, looks=looks, followers=followers, following=following, loves=loves)
