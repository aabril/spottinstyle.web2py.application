# coding: utf8
# try something like

def index():
   urluser  = request.args[0]
   urlstyle = request.args[1]

   user         = db(db.auth_user.slug==urluser).select().first()
   currentstyle = db( (db.styles.name==urlstyle) & (db.styles.owner_id==user) ).select().first()
   phototags    = db( db.photo_tags.styles==currentstyle).select(limitby=(0,49), orderby=~db.photo_tags.id)

   if db( (db.styles.name==urlstyle) & (db.styles.owner_id==user)).count()==0:
       response.view = "default/stylenotexists.html"
       return dict()
   elif  db( db.photo_tags.styles==currentstyle).count()==0:
       response.view = "default/nophotos.html"
       return dict()
   else:
       currentstyle.totalloves = db(db.loves.style_id==currentstyle.id).count()

       for phototag in phototags:
           nloves = db( (db.loves.style_id==currentstyle.id) & (db.loves.photo_id==phototag.photos.id) ).count()
           loved  = db( (db.loves.style_id==currentstyle.id) & (db.loves.photo_id==phototag.photos.id) & (db.loves.owner_id==auth.user_id) ).count()
           photos = phototag.photos
           phototag.photos["nloves"] = nloves
           phototag.photos["loved"]  = loved

       #for row in phototags.sort(lambda row: ~row.photos.nloves):
       #    print row
       #    print row.photos.nloves

       phototags = phototags.sort(lambda row: ~row.photos.nloves)

       username  = user.username
       bio       = user.bio
       followers = 0
       following = 0
       loves     = phototags.first().photos.nloves

       response.meta.description = "Liked a photo on "+urluser+" profile."

       styles = False
       tags   = False
       looks  = False

       styles = db(db.styles.owner_id==user.id).select()
       tags   = db(db.photo_tags.photos==phototags.first().photos).select()

       for tag in tags:
           tag.styles.totalloves = db(db.loves.style_id==tag.styles.id).count()
       tags = tags.sort(lambda row: ~row.styles.totalloves)

       photos = {}
       height = SIZEDHEIGHT(phototags.first().photos.width, phototags.first().photos.height)

       return dict(user=user, photos=photos, phototags=phototags, height=height, currentstyle=currentstyle, styles=styles, tags=tags, looks=looks, followers=followers, following=following, loves=loves)
