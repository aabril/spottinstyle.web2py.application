# coding: utf8
# try something like

def index():
   followers = following = loves = 0
   response.meta.description = "Liked a photo on allphotos gallery."

   styles = tags = looks = user = False
   currentstyle = photos = phototags = {}

   photos = db().select(db.photos.ALL, orderby=~db.photos.id, limitby=(0,49)) # ToDo: sort by most loved
   tags = db(db.photo_tags.photos==photos.first()).select()

   for tag in tags:
      tag.styles.totalloves = db(db.loves.style_id==tag.styles.id).count()


   for photo in photos:
       photo.totalloves = db(db.loves.photo_id==photo.id).count()

   photos = photos.sort(lambda row: ~row.totalloves)

   loves = photos.first().totalloves

   height = SIZEDHEIGHT(photos.first().width, photos.first().height)

   return dict(user=user, photos=photos, phototags=phototags, height=height, currentstyle=currentstyle, styles=styles, tags=tags, looks=looks, followers=followers, following=following, loves=loves)
