# -*- coding: utf-8 -*-

# homepage
def index():
    fbusername = stylesfollowing = False

    # automate to create the auth_user.slug 
    # put this into the db.py (or some model) as a hook
    if auth.is_logged_in():
        if not auth.user.slug:
            user = db(db.auth_user.id==auth.user.id).select().first()
            user.slug = IS_SLUG.urlify(user.username)
            user.update_record()

    if auth.is_logged_in():
        stylesfollowing = db(db.following_styles.follower==auth.user).select()
        if len(stylesfollowing)==0:
           stylesfollowing = False
        pass

    slides = db(db.slides.enabled==True).select(db.slides.ALL, orderby=db.slides.sortorder)
    #slides = db(db.slides.enabled==True).select(db.slides.ALL, orderby=db.slides.sortorder, cache=(cache.ram,3600), cacheable=True)

    return dict(slides=slides, stylesfollowing=stylesfollowing, fbusername=fbusername)

# login
def login():
    if request.vars['social_media'] == 'twitter':
        session.auth_with = 'twitter'
    if request.vars['social_media'] == 'facebook':
        session.auth_with = 'facebook'
    redirect(URL('style', 'default', 'user', args=['login?social_media=facebook']))

# login (custom to albert)
def loginbasic():
    """
    type:         basic / #facebook / #twitter
    login_next:   url to redirect after login
    username:     username
    password:     facebook
    """
    #print request.vars
    #print request.vars.username

    #if db(db.auth_user.username==request.vars.username).count() == 1:
    #    print "mola molt"

    user = auth.login_bare(request.vars.username, request.vars.password)

    redirect(request.vars._next)


# basic logout (custom by albert)
def basiclogout():
    auth.settings.logout_next = "http://"+request.env.http_host
    auth.logout()

def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """

    form = auth.login()
    # form['_class'] = "form-signin"

    # form = FORM(
    #         H2('Please sign in', _class="form-signin-heading"),
    #           INPUT(_name='name', requires=IS_NOT_EMPTY(), _class="input-block-level", _placeholder="Email Adress"),
    #           INPUT(_name='name', requires=IS_NOT_EMPTY(), _class="input-block-level", _placeholder="Password"),
    #           LABEL('Remember me', INPUT(_value="remember-me", _type="checkbox"), _class="checkbox"), 
    #           BUTTON("Sign in", _class="btn btn-large btn-primary", _type='submit'),
    #           _class="form-signin")

    # auth.settings.login_next = URL(c='panel', f='index')
    return dict(form=form)


def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_signature()
def data():
    """
    http://..../[app]/default/data/tables
    http://..../[app]/default/data/create/[table]
    http://..../[app]/default/data/read/[table]/[id]
    http://..../[app]/default/data/update/[table]/[id]
    http://..../[app]/default/data/delete/[table]/[id]
    http://..../[app]/default/data/select/[table]
    http://..../[app]/default/data/search/[table]
    but URLs must be signed, i.e. linked with
      A('table',_href=URL('data/tables',user_signature=True))
    or with the signed load operator
      LOAD('default','data.load',args='tables',ajax=True,user_signature=True)
    """
    return dict(form=crud())

def handle_error():
    """ Custom error handler that returns correct status codes."""
    
    code = request.vars.code
    request_url = request.vars.request_url
    ticket = request.vars.ticket
 
    if code is not None and request_url != request.url: # Make sure error url is not current url to avoid infinite loop.
        response.status = int(code) # Assign the error status code to the current response. (Must be integer to work.)
 
    if code == '403':
        return "Not authorized"
    elif code == '404':
        return "Not found"
    elif code == '500':
        
        # Get ticket URL:
        # ticket_url = "<a href='%(scheme)s://%(host)s/admin/default/ticket/%(ticket)s' target='_blank'>%(ticket)s</a>" % {'scheme':'https','host':request.env.http_host,'ticket':ticket}
        ticket_url = "<a href='%(scheme)s://%(host)s/admin/default/ticket/%(ticket)s' target='_blank'>ticket</a>" % {'scheme':'https','host':request.env.http_host,'ticket':ticket}
 
        # Email a notice, etc:
        # mail.send(to=['admins@myapp.com '],
        #            subject="New Error",
        #            message="Error Ticket:  %s" % ticket_url)
        
        return "Server error <br>"+ticket_url
    
    else:
        return "Other error"

