# coding: utf8
# try something like
@auth.requires_membership('adminpanel')
def index():
    users = db().select(db.auth_user.ALL, orderby=~db.auth_user.id, limitby=(0,5))
    styles = db().select(db.styles.ALL, orderby=~db.styles.id, limitby=(0,5))
    photos = db().select(db.photos.ALL, orderby=~db.photos.datetime, limitby=(0,11))

    return dict(users=users, styles=styles, photos=photos)

# Slides
@auth.requires_membership('adminpanel')
def slides():
    form   = crud.create(db.slides)
    count  = db(db.slides.id > 0).count()
    slides = db().select(db.slides.ALL, orderby=db.slides.sortorder)
    return dict(form=form, slides=slides, count=count)

@auth.requires_membership('adminpanel')
def slide():
    form = user = styles = photos = photoscount = followingusers = followedby = followingstyles = following = totalloves = False
    crud.settings.update_next = URL('panel','slides')
    if request.args[0] == 'edit':
        form    = crud.update(db.slides, request.args[1])
        slide   = db(db.slides.id==request.args[1]).select(db.slides.ALL).first()
    return dict(form=form, slide=slide)

# Users
@auth.requires_membership('adminpanel')
def users():
    # crud options
    db.auth_user.first_name.writable = db.auth_user.first_name.readable = False
    db.auth_user.last_name.writable  = db.auth_user.last_name.readable  = False
    crud.settings.create_next = URL('panel', 'users')

    form  = crud.create(db.auth_user)

    # pagination
    pagination = {}
    pagination['itemsperpage'] = 10
    try:
        pagination['offset'] = int(request.args[0])
    except:
        pagination['offset'] = 0
    pagination['endlimit'] = pagination['offset'] + pagination['itemsperpage']

    # info data
    count = db(db.auth_user.id > 0).count()
    users = db().select(db.auth_user.ALL, limitby=(pagination['offset'], pagination['endlimit']))


    # check follows
    following = []
    for follow in db(db.following_users.follower==auth.user.id).select(db.following_users.follows):
        # print follow.follows
        following.append(follow.follows)

    return dict(users=users, count=count, form=form, following=following, pagination=pagination)

@auth.requires_membership('adminpanel')
def user():
    form = user = styles = photos = photoscount = followingusers = followedby = followingstyles = following = totalloves = False
    crud.settings.update_next = URL('panel','users')
    if request.args[0] == 'edit':
        form   = crud.update(db.auth_user, request.args[1])
        user   = db(db.auth_user.id==request.args[1]).select(db.auth_user.ALL).first()

    else:
        user   = db(db.auth_user.username==request.args[0]).select(db.auth_user.ALL).first()
        styles = db(db.styles.owner_id==user.id).select(db.styles.ALL)
        photos = db(db.photos.owner_id==user.id).select(db.photos.ALL, limitby=(0,10))
        photoscount = db(db.photos.owner_id==user.id).count()


        totalloves=0
        for photo in photos:
            numloves = db(db.loves.photo_id==photo.id).count()
            totalloves = totalloves + numloves


        followingusers = {}
        for follow in db(db.following_users.follower==user.id).select(db.following_users.follows):
            # print follow.follows
            followingusers[follow.follows] = db(db.auth_user.id==follow.follows).select(db.auth_user.username).first()
        # print followingusers

        followedby = {}
        for follow in db(db.following_users.follows==user.id).select():
            print follow
            followedby[follow.follower] = db(db.auth_user.id==follow.follower).select(db.auth_user.username).first()
        # print followingusers

        followingstyles = {}
        for follow in db(db.following_styles.follower==user.id).select(db.following_styles.follows):
            # print follow.follows
            followingstyles[follow.follows] = db(db.auth_user.id==follow.follows).select(db.styles.ALL).first()

        if db((db.following_users.follower==auth.user.id)&(db.following_users.follows==user.id)).count() > 0:
            following = True

    return dict(form=form, user=user, styles=styles, photos=photos, followingusers=followingusers, followedby=followedby, followingstyles=followingstyles, following=following, totalloves=totalloves, photoscount=photoscount)

# Photos
@auth.requires_membership('adminpanel')
def photos():

    # pagination
    pagination = {}
    pagination['itemsperpage'] = 8
    try:
        pagination['offset'] = int(request.args[0])
    except:
        pagination['offset'] = 0
    pagination['endlimit'] = pagination['offset'] + pagination['itemsperpage']

    form   = crud.create(db.photos)
    count  = db(db.photos.id > 0).count()
    photos = db().select(db.photos.ALL, limitby=(pagination['offset'], pagination['endlimit']))

    for photo in photos:
        styles = db(db.photo_tags.photos==photo.id).select(db.photo_tags.ALL, limitby=(0,4))
        photo.styles = styles

    return dict(count=count, photos=photos, form=form, pagination=pagination)


# def dupefotos():
#     foto = db().select(db.photos.ALL).first()
#     for i in range(100):
#       db.photos.insert(filename=foto.filename)

@auth.requires_membership('adminpanel')
def photo():
    form = photo = tags = loves = loved = False
    crud.settings.update_next = URL('panel','photos')

    try:
        if request.args[0] == 'edit' and request.args[1] != None:
            form   = crud.update(db.photos, request.args[1])
        elif request.args[0] == 'loves' and request.args[1] != None:
            loves = db(db.loves.photo_id==request.args[1]).select(db.loves.ALL)
        elif isinstance( int(request.args[0]), int ):
            if db(db.photos.id==request.args[0]).count()==1:
                photo         = db(db.photos.id==request.args[0]).select(db.photos.ALL).first()
                photo.owner   = db(db.auth_user.id==photo.owner_id).select(db.auth_user.username).first()
                loved         = db((db.loves.owner_id==auth.user.id) & (db.loves.photo_id==photo.id)).count() > 0
                #loves         = db(db.loves.photo_id==request.args[0]).select(db.loves.ALL, orderby=~db.loves.id, limitby=(0,5))
                loves         = db(db.loves.photo_id==request.args[0]).count()
                tags          = db(db.photo_tags.photos==photo.id).select(db.photo_tags.ALL)
            else:
                pass
    except:
        redirect(URL('panel','photos'))

    return dict(form=form, photo=photo, tags=tags, loves=loves, loved=loved)

# Galleries
@auth.requires_membership('adminpanel')
def galleries():
    form      = crud.create(db.galleries)
    crud.settings.create_next = URL('panel', 'galleries')
    count     = db(db.galleries.id > 0).count()
    galleries = db().select(db.galleries.ALL)
    response.flash = T("Welcome to web2py!")
    return dict(form=form, galleries=galleries, count=count)

@auth.requires_membership('adminpanel')
def gallery():
    form    = None
    gallery = None

    # Guess if we should show or edit
    if request.args(0) == 'edit':
        argument = 'edit'
    else:
        argument = int(request.args(0))

    # Guess if show, edit or display missing parameter
    if argument == 'edit' :
        if request.args(1) != None:
            form = crud.update(db.galleries, request.args(1))
        else:
            form = "Can't edit any gallery because any parameter is given."
    elif type(argument) is int :
        gallery = db(db.galleries._id==argument).select()

    return dict(form=form, gallery=gallery)

# Galleries
@auth.requires_membership('adminpanel')
def styles():
    form = styles = count = following = False

    # pagination
    pagination = {}
    pagination['itemsperpage'] = 10
    try:
        pagination['offset'] = int(request.args[0])
    except:
        pagination['offset'] = 0
    pagination['endlimit'] = pagination['offset'] + pagination['itemsperpage']


    form      = crud.create(db.styles)
    crud.settings.create_next = URL('panel', 'styles')
    count     = db(db.styles.id > 0).count()
    styles    = db().select(db.styles.ALL, limitby=(pagination['offset'], pagination['endlimit']))

    following = []
    for follow in db(db.following_styles.follower==db.auth_user.id).select(db.following_styles.follows):
        following.append(follow.follows)

    for style in styles:
        style['nphotos'] = db(db.photo_tags.styles==style.id).count()

        # Get Sum of loves
        photos = db(db.photo_tags.styles==style.id).select()
        sumloves = 0
        for photo in photos:
            photo.loves = db(db.loves.photo_id==photo.photos).count()
            sumloves = sumloves + photo.loves
        style['sumloves'] = sumloves


    return dict(form=form, styles=styles, count=count, following=following, pagination=pagination)


@auth.requires_membership('adminpanel')
def style():
    style = owner = nphotos = photos = False

    style = db(db.styles.id==int(request.args[0])).select().first()
    photos = db(db.photo_tags.styles==style.id).select()
    owner = db(db.auth_user.id==style.owner_id).select().first()
    following = db( (db.following_styles.follower==auth.user.id) & (db.following_styles.follows==style.id) ).count() > 0

    sumloves = 0
    for photo in photos:
        photo.loves = db(db.loves.photo_id==photo.photos).count()
        sumloves = sumloves + photo.loves

    return dict(style=style, photos=photos, owner=owner, nphotos=nphotos, sumloves=sumloves, following=following)


@auth.requires_membership('adminpanel')
def looks():
    return dict()

# AJAX
@auth.requires_membership('adminpanel')
def _follow():
    if request.vars.type == "style":
        db.following_styles.insert(follower=int(request.vars.follower), follows=int(request.vars.following))
    else:
        db.following_users.insert(follower=int(request.vars.follower), follows=int(request.vars.following))

    return dict()

@auth.requires_membership('adminpanel')
def _unfollow():
    if request.vars.type == "style":
        db((db.following_styles.follower==request.vars.follower) & (db.following_styles.follows==request.vars.following)).delete()
    else:
        db((db.following_users.follower==request.vars.follower) & (db.following_users.follows==request.vars.following)).delete()

    return dict()

@auth.requires_membership('adminpanel')
def _love():
    print "hola"
    print request.vars
    db.loves.insert(owner_id=request.vars.user, photo_id=request.vars.love)
    return dict()

@auth.requires_membership('adminpanel')
def _unlove():
    # print request.vars
    # print auth.user.id
    db((db.loves.owner_id==auth.user.id) & (db.loves.photo_id==request.vars.love)).delete()
    return dict()

#@auth.requires_membership('adminpanel')
# def _stylesearch():
#     print request.vars

#     print db(db.styles.name.startswith(request.vars.keyword)).select().first()
#     # print db(db.styles.name.contains('value')).select()


#     # print auth.user.id
#     return dict()

@auth.requires_membership('adminpanel')
@service.jsonrpc
def _stylesearch():
    styles = db(db.styles.name.startswith(request.vars.keyword)).select()

    for style in styles:
        style.name = style.owner_id.username + '/' + style.name

        # Get the sum of loves of the style
        sumloves   = 0
        photo_tags = db(db.photo_tags.styles==style.id).select()

        if len(photo_tags)==0:
            style.description = 0
        else:
            for tag in photo_tags:
                numloves          = db(db.loves.photo_id==tag.photos.id).count()
                sumloves          = sumloves + numloves
                style.description = int(sumloves)

    sortbyloves = styles.sort(lambda row: row.description)
    styles = XML(sortbyloves.json())

    return styles

@auth.requires_membership('adminpanel')
def _stylecreate():

    results = db((db.styles.owner_id==auth.user.id)&(db.styles.name==request.vars.newstyle)).count()
    if results > 0:
        print "this style already exists in the database"
    else:
        # print "creando nuevo"
        newstyleid = db.styles.insert(name=request.vars.newstyle, owner_id=auth.user.id, description="")
        # print "newstyleid "+str(newstyleid)
        db.photo_tags.insert(author=auth.user.id, photos=request.vars.photoid, styles=newstyleid)

    # if db((db.photo_tags.author==auth.user.id)&(db.photo_tags.styles.name==request.vars.newstyle)).count() > 0:
    #     print "ya existe"
    # else:
    #     print "creando nuevo"
    #     newstyleid = db.styles.insert(name=request.vars.newstyle, owner_id=auth.user.id, description="")
    #     print "newstyleid "+str(newstyleid)
    #     db.photo_tags.insert(author=auth.user.id, photos=request.vars.photoid, styles=newstyleid)



    return dict()



@auth.requires_membership('adminpanel')
def _tagstyle():
    print request.vars
    print auth.user.id
    # print auth.user.id

    print "select"
    db((db.photo_tags.photos==request.vars.photoid)&(db.photo_tags.styles==request.vars.styleid))

    # ToDo: check if already exists
    if db((db.photo_tags.photos==request.vars.photoid)&(db.photo_tags.styles==request.vars.styleid)).count() > 0:
        print "this photo is already tagged to this style"
    else:
        db.photo_tags.insert(author=auth.user, photos=request.vars.photoid, styles=request.vars.styleid)

    return dict()


@auth.requires_membership('adminpanel')
def _deletestyle():
    print request.vars
    #todo: cahnge name to :delete photo_tag
    print "deletestyletag"
    print auth.user.id
    print db((db.photo_tags.author==auth.user.id) & (db.photo_tags.styles==request.vars.styleid)).select()
    print db((db.photo_tags.author==auth.user.id) & (db.photo_tags.styles==request.vars.styleid)).delete()
    return dict()


# def borra():
#     for item in db(db.styles.id > 22).select():
#         db(db.styles.id==item.id).delete()
#     redirect(URL('panel','styles'))


# def addusers():
#     dummyuser = db().select(db.auth_user.ALL, orderby=~db.auth_user.id).first()
#     print dummyuser.username
#     for i in range(14,500):
#         dummyuser.first_name = "aa"+str(i)
#         dummyuser.last_name = "aa"+str(i)
#         dummyuser.username = "aa"+str(i)
#         dummyuser.email = "aa"+str(i)+"@blabla.com"
#         db.auth_user.insert(first_name=dummyuser.first_name, last_name=dummyuser.last_name, username=dummyuser.username, email=dummyuser.email, avatar=dummyuser.avatar, password=dummyuser.password)
#     #redirect(URL('panel','styles'))

@auth.requires_membership('adminpanel')
def posts():
	return dict()

@auth.requires_membership('adminpanel')
def events():
	return dict()

@auth.requires_membership('adminpanel')
def videos():
	return dict()

@auth.requires_membership('adminpanel')
def loves():
	return dict()

def logout():
    auth.logout(next=URL('default','index'))
    return dict()
