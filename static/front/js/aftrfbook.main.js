$( document ).ready(function() {

//==========================================================================
////////////////////////////////////////////////////////////////////////////
//==========================================================================
// CONTEXT BOX
//==========================================================================
////////////////////////////////////////////////////////////////////////////
//==========================================================================

//if($('.afterfacebook').length!=0){
//   $('.contextBox').toggle();
//};

//after facebook # check username
//$('#checkUsername').on("keyup", function(){
//    //console.log($('#checkUsername').val().length);
//    if($('#checkUsername').val().length > 3){
//        addchecksymbol();
//    }else{
//        $('#available').html('');
//    };
//    //console.log("hehe");
//    //console.log($('#checkUsername').val());
//});

//if ($('#checkUsername').length != 0){
//addchecksymbol();
//} 

//$("#checkUsername").on("keyup", function(){
//    addchecksymbol();
//});


//function addchecksymbol(){
//   var value = $('#checkUsername').val();
//
//   var formattedvalue = value.replace(/\W+/g,""); //remove non-alphanumeric characters and substitue by nospace
//
//   $('#checkUsername').val(formattedvalue);
//
//   $.ajax({
//      url: "http://beta.spottinstyle.com/ajax/userexists",
//      type: "POST",
//      data: {"user": formattedvalue},
//      success: function(data, textStatus, jqXHR){
//        //console.log(data);
//        if(data==0){
//          //console.log("no existe");
//          $('#available').html("available icon | submit button"); 
//        }else if(data==1){
//          //console.log("si existe");
//          $('#available').html("no available icon"); 
//        }
//      },
//      error: function(data, textStatus, jqXHR){
//      },
//   });
//
//};



  $('#loginButton').click(function(){

    function login(){
	if ( $('.contextBox').is(":visible")  ){
               $('.signUp').css("display",'none');
               $('.login').css("display",'none');
               $('.logA').css('display', 'inline-block');           
        }else if( $('.contextBox').is(":visible") ) {
            $('.contextBox').slideUp('slow');
            $('#loginButton').removeClass("activeButton"); 

        }else{
            $('#loginButton').addClass("activeButton"); 
            $("#logSignIn").css('display', 'block');
            $('.contextBox').slideDown('slow');
        }
    };
 
    if ( $("#upload").is(":visible") ) {
          $( "#uploadButton" ).removeClass( "activeButton" );
          $( "#loginButton" ).addClass( "activeButton" );
          $('.contextBox').slideUp("slow");
          document.getElementById('upload').style.display = 'none';
          timeoutID = window.setTimeout(login, 500);
          $('.contextBox').slideDown("slow");
          $('#loginButton').addClass("activeButton");
    } else if ( $("#searchResults").is(":visible") ) {
          $('.contextBox').slideUp("slow");
          document.getElementById('searchResults').style.display = 'none';
          timeoutID = window.setTimeout(login, 500);
    } else {
          var funclogin = login();
    }
  });

//SHOW LOGIN DIALOG

  $("#logButton").click(function(event) {
    event.preventDefault();
    $('.logA').fadeToggle();
    timeoutID = window.setTimeout(fadeLog, 400);
  });

//SHOW SIGN UP DIALOG

  $("#signButton").click(function(event) {
    event.preventDefault();
    $('.logA').fadeToggle();
    timeoutID = window.setTimeout(fadeSign, 400);
  });

//==========================================================================
// UPLOAD PHOTO
//==========================================================================

  $('#uploadButton').click(function(){
    function upload(){
        if ( $("#upload").is(":visible") ) {
          $('#upload').slideUp("slow");
          $( "#uploadButton" ).removeClass( "activeButton" );
        } else {
          $( "#uploadButton" ).addClass( "activeButton" );
          document.getElementById('upload').style.display = 'block';
        }
        $('.contextBox').slideToggle("slow");
      }

    if ( $("#logSignIn").is(":visible") ) {
      $( "#loginButton" ).removeClass( "activeButton" );
      $( "#uploadButton" ).addClass( "activeButton" );
      $('.contextBox').slideUp(490);
      document.getElementById('logSignIn').style.display = 'none';
      timeoutID = window.setTimeout(upload, 500);
    } else if ( $("#searchResults").is(":visible") ) {
      $('.contextBox').slideUp(490);
      document.getElementById('searchResults').style.display = 'none';
      timeoutID = window.setTimeout(upload, 500);
    } else {
      var funcupload = upload();
    }
  });

//SEARCH STYLES AND TAG TO PHOTO UPLOAD

  $("input[name=tagUpload]").bind("change paste keyup", function() {

    var searchText = $(this).val();
    console.log(searchText); 
    //console.log(searchText.length); 

    if( searchText.length == 0 ){
      //console.log("Search text is null");
      $('#upload .styleBox').slideUp("slow");
    }else{
      $('#upload .styleBox').slideDown("slow");
    }   

  });

//LOGIN BUTTON IF USER NOT LOGGED IN

  $( "#loginButtonTrigger" ).click(function() {
    $( "#loginButton" ).click();
  });

//==========================================================================
// SEARCH SITE
//==========================================================================

  $("input[name=search]").bind("change paste keyup", function() {

    var searchText = $(this).val();
    console.log(searchText); 
    //console.log(searchText.length); 

    if( searchText.length == 0 ){
      //console.log("Search text is null");
      $('.contextBox').slideUp("slow");
      $('#searchResults').slideUp("slow");
      $( "#loginButton" ).removeClass( "activeButton" );
      $( "#uploadButton" ).removeClass( "activeButton" );
    }else{
      function search(){
        document.getElementById('searchResults').style.display = 'block';
        $('.contextBox').slideDown("slow");
      }
      if ( $("#logSignIn").is(":visible") ) {
        $( "#loginButton" ).removeClass( "activeButton" );
        $('.contextBox').slideUp(490);
        document.getElementById('logSignIn').style.display = 'none';
        timeoutID = window.setTimeout(search, 500);
      }else if ( $("#upload").is(":visible") ) {
        $( "#uploadButton" ).removeClass( "activeButton" );
        $('.contextBox').slideUp(490);
        document.getElementById('upload').style.display = 'none';
        timeoutID = window.setTimeout(search, 500);
      }else {
        var funcsearch = search();
      }

      window.swipeSearchStyle = new Swipe(document.getElementById('swipeSearchStyle'), {
        startSlide: 0,
        speed: 1000,
        auto: false,
        continuous: true,
        disableScroll: false,
        stopPropagation: false,
        callback: function(pos) {},
        transitionEnd: function(index, elem) {}
      });

      window.swipeSearchUser = new Swipe(document.getElementById('swipeSearchUser'), {
        startSlide: 0,
        speed: 1000,
        auto: false,
        continuous: true,
        disableScroll: false,
        stopPropagation: false,
        callback: function(pos) {},
        transitionEnd: function(index, elem) {}
      });
    }

  });

  $("input[name=tagStyle]").bind("change paste keyup", function() {

    var searchText = $(this).val();
    console.log(searchText); 
    //console.log(searchText.length); 

    if( searchText.length == 0 ){
      //console.log("Search text is null");
      $('#swipeTagStyle').parent().slideUp("slow");
    }else{
      $('#swipeTagStyle').parent().slideDown("slow", function() {

        window.swipeTagStyle = new Swipe(document.getElementById('swipeTagStyle'), {
          startSlide: 0,
          speed: 1000,
          auto: false,
          continuous: true,
          disableScroll: false,
          stopPropagation: false,
          callback: function(pos) {},
          transitionEnd: function(index, elem) {}
        });

      });
    }   

  });

  $(".love").click(function () {
    $('.loveBox').slideToggle();
  });

//REMOVE FEED FROM HOME PAGE

  $(".removeStyleFeed").click(function(event) {
    event.preventDefault();
    $(this).parent().fadeOut();
  });



//SHOW/HIDE COMMENTS
    
    $('.commentButton').click(function(event) {
    event.preventDefault();  
    $('.comments').slideToggle();
  });


//==========================================================================
// LOVE BUTTON
//==========================================================================

//ANIMATE HEART ICON AND SEND LOVE AROUND THE WORLD (AJAX)

$(".love").click(function(event) {
  $(".loveHeart .icon-heart-empty").addClass("icon-heart");
  $(".loveHeart .icon-heart-empty").addClass("pulse");
  $(".loveHeart .icon-heart-empty").removeClass("icon-heart-empty");

  $.ajax({
    type: "POST",
    url: "some.php",
    data: { user: "user id", photo: "this photo id" }
  })
  .done(function( msg ) {
    alert( "Data Saved: " + msg );
  });

});

function fadeLog(){
  $('.login').fadeToggle("slow");
}

function fadeSign(){
  $('.signUp').fadeToggle("slow");
}

$( "#tabs" ).tabs();

});

//==========================================================================
////////////////////////////////////////////////////////////////////////////
//==========================================================================
// TAGS
//==========================================================================
////////////////////////////////////////////////////////////////////////////
//==========================================================================

$('#showTagCreator').click(function(event) {
  event.preventDefault();
  if ( $(".lookCreator").is(":visible") ) {
    $('.lookCreator').slideUp();
    $('.photo .tagStyle').slideToggle();
  }else {
    $('.photo .tagStyle').slideToggle();
  }
});

//==========================================================================
// SELECT TAGS
//==========================================================================

//DECLARE VARIABLES

var style_id = null;

//SELECT STYLE TO TAG (FOR UPLOAD)

$("#upload .styleBox .divLink").click(function(event) {
  event.preventDefault();
  var style_bg = $(this).parent();
  console.log($(style_bg).css("background-color"));

  if($(style_bg).hasClass("selected")) {
    $(style_bg).removeClass("selected");
  }else {
    $("#upload .styleBox").children(".inline").removeClass("selected");
    $(style_bg).addClass("selected");
  }
});

//SELECT STYLE TO TAG (FOR CURRENT PHOTO) AND CHANGE VALUE FOR SUBMIT BUTTON

$(".photo .styleBox .divLink").click(function(event) {
  event.preventDefault();
  var style_bg = $(this).parent();
  var selected = false;

  //GET STYLE ID
  style_id = $(this).attr("id");

  if($(style_bg).hasClass("selected")) {
    $(style_bg).removeClass("selected");
    selected = false;
  }else {
    $(".photo .styleBox div").children(".inline").removeClass("selected");
    $(style_bg).addClass("selected");
    $("#tagStyleButton").val("Tag Style");
    selected = true;
  }

  if(!selected){
    $("#tagStyleButton").val("Create new Style");
  }

});

//==========================================================================
// CREATE NEW STYLE TO TAG
//==========================================================================

//SUBMIT BUTTON CLICK FUNCTION

$('.tagStyle #tagStyleButton').click(function(event) {
  event.preventDefault();

  if($(this).val() == "Create new Style") {

    //SHOW DIALOG FOR NEW STYLE
    $(".blackOverlay").fadeIn();
    timeoutID = window.setTimeout(newStyleBox, 800);
    function newStyleBox() {
      $('.newStyle').fadeIn(800);
    }
  }else {

    //TAG STYLE ID TO PHOTO ID (AJAX)
    $.ajax({
      type: "POST",
      url: "some.php",
      data: { style: style_id, photo: "photo id" }
    })
    .done(function( msg ) {
      alert( "Data Saved: " + msg );
    });
  }
  
});

//SUBMIT THE NEW STYLE (AJAX) AND FADE OUT DIALOG

$('.newStyle input[name="createStyle"]').click(function(event) {
  event.preventDefault();

  $.ajax({
    type: "POST",
    url: "some.php",
    data: { title: "John", description: "Boston", photo: "this photo id" }
  })
  .done(function( msg ) {
    alert( "Data Saved: " + msg );
  });

  $('.newStyle').fadeOut();
  $(".blackOverlay").fadeOut();
});

//CANCEL DIALOG

$('#cancelNewStyle').click(function(event) {
  event.preventDefault();
    $('.newStyle').fadeOut();
    $(".blackOverlay").fadeOut();
});


//==========================================================================
////////////////////////////////////////////////////////////////////////////
//==========================================================================
// LOOKS
//==========================================================================
////////////////////////////////////////////////////////////////////////////
//==========================================================================

//SHOW LOOK CREATOR

  $('#showLookCreator').click(function(event) {
    if ( $(".photo .tagStyle").is(":visible") ) {
      $('.photo .tagStyle').slideUp();
      $('.lookCreator').slideToggle();
    }else {
      $('.lookCreator').slideToggle();
    }
  });

//HIDE LOOK CREATOR

  $('#hideLookCreator').click(function(event) {
    event.preventDefault();
    $('.lookCreator').slideUp(function() {
      // Animation complete.
    });
  });

//MAKE DROP BOX TARGET FOR DRAG AND DROP ACTION

  $( ".dropBox" ).droppable({
    accept: '.productThumb',
    drop: function( event, ui ) {
      
      $( this ).find( "span" ).remove();
      $(this).append($(ui.draggable).clone());
      $(".dropBox .productThumb").addClass("item");
      $(".item").removeClass("ui-draggable productThumb");

      $(".item").click(function(event) {
        event.preventDefault();
      }).draggable ({
        stack: ".dropBox .item",
        containment: ".dropBox",
        scroll: false
      }).resizable({aspectRatio: 1/1, minWidth: 65});

    }
  });

//MAKE PRODUCTS DRAGGABLE

  $(".lookProducts .productThumb").click(function(event) {
    event.preventDefault();
  }).draggable({ 
    stack: ".lookProducts .productThumb", 
    revert: "invalid", 
    scroll: false, 
    helper: 'clone'
  });

window.swipePhoto = new Swipe(document.getElementById('swipePhoto'), {
  startSlide: 0,
  speed: 1000,
  auto: false,
  continuous: true,
  disableScroll: false,
  stopPropagation: false,
  callback: function(pos) {},
  transitionEnd: function(index, elem) {}
});


//==========================================================================
// HOME PAGE BANNER (SWIPE)
//==========================================================================

window.mySwipe = new Swipe(document.getElementById('slider'), {
  startSlide: 0,
  speed: 1000,
  auto: 4000,
  continuous: true,
  disableScroll: false,
  stopPropagation: false,
  callback: function(pos) {
      var i = bullets.length;
      while (i--) {
        bullets[i].className = '';
      }
      bullets[pos].className = 'on';
      },
  transitionEnd: function(index, elem) {}
});

var bullets = document.getElementById('position').getElementsByTagName('li');



