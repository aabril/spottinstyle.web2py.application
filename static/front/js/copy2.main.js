$( document ).ready(function() {


 // Disabling functions
 //$(':input.login').parent().click(function(){ alert("function disabled"); });
 $('.register').parent().click(function(){ alert("function disabled"); });
 
 
 
 //==========================================================================
 ////////////////////////////////////////////////////////////////////////////
 //==========================================================================
 // CanJS Route ( http://canjs.com/guides/Routing.html  ) 
 //==========================================================================
 ////////////////////////////////////////////////////////////////////////////
 //==========================================================================
 //window.location.hash = '#';
 //can.route.attr();
 
 
 
 
 
 //==========================================================================
 ////////////////////////////////////////////////////////////////////////////
 //==========================================================================
 // CONTEXT BOX
 //==========================================================================
 ////////////////////////////////////////////////////////////////////////////
 //==========================================================================

  if($('.afterfacebook').length!=0){
     $('.contextBox').toggle();
  };
  var initial_login = true;

  $('#loginButton').click(function(){
    function login(){
        if ( $("#logSignIn").is(":visible") ) {
          $('#logSignIn').slideUp("slow");
          $( "#loginButton" ).removeClass( "activeButton" );
        } else {
          $( "#loginButton" ).addClass( "activeButton" );
          
          //CHECK IF INITIAL LOGIN DIALOG IS OPEN, IF NOT SHOW IT

          if (initial_login === false) {
            $(".login").css("display", "none");
            $(".signUp").css("display", "none");
            $(".logA").css("display", "inline-block");
          }
          document.getElementById('logSignIn').style.display = 'block';
        }
        $('.contextBox').slideToggle("slow");

      }
      
    if ( $("#upload").is(":visible") ) {
      $( "#uploadButton" ).removeClass( "activeButton" );
      $( "#loginButton" ).addClass( "activeButton" );
      $('.contextBox').slideUp("slow");
      document.getElementById('upload').style.display = 'none';
      timeoutID = window.setTimeout(login, 500);
    } else if ( $("#searchResults").is(":visible") ) {
      $('.contextBox').slideUp("slow");
      document.getElementById('searchResults').style.display = 'none';
      timeoutID = window.setTimeout(login, 500);
    } else {
      var funclogin = login();
    }
  });

  //SHOW LOGIN DIALOG

  $("#logButton").click(function(event) {
    event.preventDefault();
    $('.logA').fadeToggle();
    timeoutID = window.setTimeout(fadeLog, 400);

    function fadeLog(){
      $('.login').fadeToggle("slow");
    }

    initial_login = false;

  });

  //SHOW SIGN UP DIALOG

  $("#signButton").click(function(event) {
    event.preventDefault();
    $('.logA').fadeToggle();
    timeoutID = window.setTimeout(fadeSign, 400);

    function fadeSign(){
      $('.signUp').fadeToggle("slow");
    }

    initial_login = false;

  });


//==========================================================================
// UPLOAD PHOTO
//==========================================================================

  $('#uploadButton').click(function(){
    function upload(){
        if ( $("#upload").is(":visible") ) {
          $('#upload').slideUp("slow");
          $( "#uploadButton" ).removeClass( "activeButton" );
        } else {
          $( "#uploadButton" ).addClass( "activeButton" );
          document.getElementById('upload').style.display = 'block';
        }
        $('.contextBox').slideToggle("slow");
      }
    if ( $("#logSignIn").is(":visible") ) {
      $( "#loginButton" ).removeClass( "activeButton" );
      $( "#uploadButton" ).addClass( "activeButton" );
      $('.contextBox').slideUp(490);
      document.getElementById('logSignIn').style.display = 'none';
      timeoutID = window.setTimeout(upload, 500);
    } else if ( $("#searchResults").is(":visible") ) {
      $('.contextBox').slideUp(490);
      document.getElementById('searchResults').style.display = 'none';
      timeoutID = window.setTimeout(upload, 500);
    } else {
      var funcupload = upload();
    }
  });

//SEARCH STYLES AND TAG TO PHOTO UPLOAD

  $("input[name=tagUpload]").bind("change paste keyup", function() {

    var searchText = $(this).val();

    console.log(searchText); 
    //console.log(searchText.length); 

    //console.log(window.location.href);    

    if( searchText.length == 0 ){
      //console.log("Search text is null");
      $('#upload .styleBox').slideUp("slow");
    }else{
      $('#upload .styleBox').slideDown("slow");
    }   

  });

//LOGIN BUTTON IF USER NOT LOGGED IN

  $( "#loginButtonTrigger" ).click(function() {
    $( "#loginButton" ).click();
  });

//==========================================================================
// SEARCH SITE
//==========================================================================

  function usernametoslug(username){
     var sluglink = username.replace(".", "");
     sluglink = sluglink.toLowerCase();
     return sluglink;
  }


  $("input[name=search]").typing({
       start: function (){
         //$('#styleresults').html('<img src="http://24.media.tumblr.com/5eee793d1d2139bcfb998c14159a30c6/tumblr_mhd7d95I2Y1qk2z5wo1_500.gif" />');
         //$('#searchresults').html('<img src="http://bradfordchamber.com/Templates/img/loading.gif" />');
         $('#styleresults').html('<img style="margin: 80px 220px;" src="/style/static/front/img/preview.gif" />');
         $('#userresults').html('');
         //console.log("start typing");
       },
       stop: function(){
          //console.log("stopped typing");
          var results = false; 

          if( $('.search').val().length > 0 ){
              var request = $.ajax({
                  url: "/ajax/search",
                  type: "post",
                  data: {search: $('.search').val() },
                  dataType: "JSON",
              });
          }

          $('#styleresults').html('');
          $('#userresults').html('');

          request.done(function (results){ 
               var styles = results.styles;   
               var users  = results.users;   
               //console.log(styles);
               //console.log(users);
              
               // Style Results 
               if(Object.keys(styles).length >0){
                  //console.log("aaa");
                  for(i=0; i<Object.keys(styles).length ; i++ ){

                       var item = can.view("searchresultStyleItemEJS", 
                                           { stylename: styles[i].name  , 
                                             owner: styles[i].owner , 
                                             sluglink: usernametoslug(styles[i].owner) , 
                                             numloves: styles[i].loves , 
                                             thumb: styles[i].thumb  });
                       $('#styleresults').append(item);
                  };
               }else{
                  //console.log("bbb");
                  $('#styleresults').append("<h4 style='margin-left: 40px;'>No styles found.</h4>");
               }

               // User Results
               if(Object.keys(users).length >0){
                  //console.log("aaa");
                  for(i=0; i<Object.keys(users).length ; i++ ){
                       var item = can.view("searchresultUserItemEJS",  
                                           { username: users[i].username , 
                                             sluglink: usernametoslug(users[i].username) ,
                                             followers: users[i].followers , 
                                             following: users[i].following  });
                       $('#userresults').append(item);
                  };
               }else{
                  //console.log("bbb");
                  $('#userresults').append("<h4 style='margin-left: 40px;'>No users found.</h4>");
               }

          });

          

       },
       delay: 600,
  });

  $("input[name=search]").bind("change paste keyup", function() {

    var searchText = $(this).val();
 
    if( searchText.length == 0 ){
      //console.log("Search text is null");
      $('.contextBox').slideUp("slow");
      $('#searchResults').slideUp("slow");
      $( "#loginButton" ).removeClass( "activeButton" );
      $( "#uploadButton" ).removeClass( "activeButton" );
    }else{
      function search(){
        document.getElementById('searchResults').style.display = 'block';
        $('.contextBox').slideDown("slow");
      }
      if ( $("#logSignIn").is(":visible") ) {
        $( "#loginButton" ).removeClass( "activeButton" );
        $('.contextBox').slideUp(490);
        document.getElementById('logSignIn').style.display = 'none';
        timeoutID = window.setTimeout(search, 500);
      }else if ( $("#upload").is(":visible") ) {
        $( "#uploadButton" ).removeClass( "activeButton" );
        $('.contextBox').slideUp(490);
        document.getElementById('upload').style.display = 'none';
        timeoutID = window.setTimeout(search, 500);
      }else {
        var funcsearch = search();
      }

      window.swipeSearchStyle = new Swipe(document.getElementById('swipeSearchStyle'), {
        startSlide: 0,
        speed: 1000,
        auto: false,
        continuous: true,
        disableScroll: false,
        stopPropagation: false,
        callback: function(pos) {},
        transitionEnd: function(index, elem) {}
      });

      window.swipeSearchUser = new Swipe(document.getElementById('swipeSearchUser'), {
        startSlide: 0,
        speed: 1000,
        auto: false,
        continuous: true,
        disableScroll: false,
        stopPropagation: false,
        callback: function(pos) {},
        transitionEnd: function(index, elem) {}
      });
    }

  });

  $(".photo input[name=tagStyle]").bind("change paste keyup", function() {

    var searchText = $(this).val();
    //console.log(searchText); 
    //console.log(searchText.length); 

    if( searchText.length == 0 ){
      //console.log("Search text is null");
      $('.photo #swipeTagStyle').parent().slideUp("slow");
    }else{
      $('.photo #swipeTagStyle').parent().slideDown("slow", function() {

        $.post('search.php', {searchReq : searchText}, function(data){

          $.each(data, function(index, value) {

            $(".styleBox").append(value);

          });

          var divs = $(".styleBox > div");

          for(var i = 0; i < divs.length; i+=4) {
            divs.slice(i, i+4).wrapAll("<div></div>");
          }

        },'json');

        window.swipeTagStyle = new Swipe($(".photo #swipeTagStyle").get(0), {
          startSlide: 0,
          speed: 1000,
          auto: false,
          continuous: true,
          disableScroll: false,
          stopPropagation: false,
          callback: function(pos) {},
          transitionEnd: function(index, elem) {}
        });

      });
    }   

  });

  $("#upload input[name=tagStyle]").bind("change paste keyup", function() {

    var searchText = $(this).val();
    //console.log(searchText); 
    //console.log(searchText.length); 

    if( searchText.length == 0 ){
      //console.log("Search text is null");
      $('#upload #swipeTagUpload').parent().slideUp("slow");
    }else{
      $('#upload #swipeTagUpload').parent().slideDown("slow", function() {

        $.post('search.php', {searchReq : searchText}, function(data){

          $.each(data, function(index, value) {

            $(".styleBox").append(value);

          });

          var divs = $(".styleBox > div");

          for(var i = 0; i < divs.length; i+=4) {
            divs.slice(i, i+4).wrapAll("<div></div>");
          }

        },'json');

        window.swipeTagUpload = new Swipe($("#upload #swipeTagUpload").get(0), {
          startSlide: 0,
          speed: 1000,
          auto: false,
          continuous: true,
          disableScroll: false,
          stopPropagation: false,
          callback: function(pos) {},
          transitionEnd: function(index, elem) {}
        });

      });
    }   

  });






//SHOW/HIDE COMMENTS
	
	$('.commentButton').click(function(event) {
    event.preventDefault();  
    $('.comments').slideToggle();
  });


//==========================================================================
// LOVE BUTTON
//==========================================================================

//ANIMATE HEART ICON AND SEND LOVE AROUND THE WORLD (AJAX)

$(".love").click(function(event) {

  if( $(".loveHeart").children().hasClass("icon-heart-empty") ){
     $('#nloves').html(parseInt($('#nloves').html())+1);
  }else{
     $('#nloves').html(parseInt($('#nloves').html())-1); 
  };


  $(".loveHeart").children().toggleClass("icon-heart");
  $(".loveHeart").children().toggleClass("icon-heart-empty");
  $(".loveHeart").children().toggleClass("pulse");

  $.ajax({
    type: "POST",
    url: "some.php",
    data: { user: "user id", photo: "this photo id" }
  })
  .done(function( msg ) {
    alert( "Data Saved: " + msg );
  });

});

  $(".love").click(function () {
    $('.loveBox').slideToggle();
  });

$( "#tabs" ).tabs();

//});

//==========================================================================
////////////////////////////////////////////////////////////////////////////
//==========================================================================
// TAGS
//==========================================================================
////////////////////////////////////////////////////////////////////////////
//==========================================================================

$('#showTagCreator').click(function(event) {
  event.preventDefault();
  if ( $(".lookCreator").is(":visible") ) {
    $('.lookCreator').slideUp();
    $('.photo .tagStyle').slideToggle();
  }else {
    $('.photo .tagStyle').slideToggle();
    $('#tagsInput').focus();
  }
});

//==========================================================================
// SELECT TAGS
//==========================================================================

//DECLARE VARIABLES

var style_id = null;

//SELECT STYLE TO TAG (FOR UPLOAD)

$("#upload .styleBox .divLink").click(function(event) {
  event.preventDefault();
  var style_bg = $(this).parent();
  var selected = false;

  //GET STYLE ID
  style_id = $(this).attr("id");

  if($(style_bg).hasClass("selected")) {
    $(style_bg).removeClass("selected");
    selected = false;
  }else {
    $("#upload .styleBox div").children(".inline").removeClass("selected");
    $(style_bg).addClass("selected");
    $("#tagUploadButton").val("Tag Style");
    selected = true;
  }

  if(!selected){
    $("#tagUploadButton").val("Create new Style");
  }

});

//SELECT STYLE TO TAG (FOR CURRENT PHOTO) AND CHANGE VALUE FOR SUBMIT BUTTON

$(".photo .styleBox .divLink").click(function(event) {
  event.preventDefault();
  var style_bg = $(this).parent();
  var selected = false;

  //GET STYLE ID
  style_id = $(this).attr("id");

  if($(style_bg).hasClass("selected")) {
    $(style_bg).removeClass("selected");
    selected = false;
  }else {
    $(".photo .styleBox div").children(".inline").removeClass("selected");
    $(style_bg).addClass("selected");
    $("#tagStyleButton").val("Tag Style");
    selected = true;
  }

  if(!selected){
    $("#tagStyleButton").val("Create new Style");
  }

});

//==========================================================================
// CREATE NEW STYLE TO TAG
//==========================================================================

//SUBMIT BUTTON CLICK FUNCTION

$('#tagStyleButton').click(function(event) {
  event.preventDefault();

  if($(this).val() == "Create new Style") {

    //SHOW DIALOG FOR NEW STYLE
    $(".blackOverlay").fadeIn();
    timeoutID = window.setTimeout(newStyleBox, 800);
    function newStyleBox() {
      $('.newStyle').fadeIn(800);
    }
  }else {

    //TAG STYLE ID TO PHOTO ID (AJAX)
    $.ajax({
      type: "POST",
      url: "some.php",
      data: { style: style_id, photo: "photo id" }
    })
    .done(function( msg ) {
      alert( "Data Saved: " + msg );
    });
  }
  
});

//SUBMIT BUTTON CLICK FUNCTION

$('#tagUploadButton').click(function(event) {
  event.preventDefault();

  if($(this).val() == "Create new Style") {

    //SHOW DIALOG FOR NEW STYLE
    $(".blackOverlay").fadeIn();
    timeoutID = window.setTimeout(newStyleBox, 800);
    function newStyleBox() {
      $('.newStyle').fadeIn(800);
    }
  }else {

    //TAG STYLE ID TO PHOTO ID (AJAX)
    $.ajax({
      type: "POST",
      url: "some.php",
      data: { style: style_id, photo: "photo id" }
    })
    .done(function( msg ) {
      alert( "Data Saved: " + msg );
    });
  }
  
});

//SUBMIT THE NEW STYLE (AJAX) AND FADE OUT DIALOG

$('.newStyle input[name="createStyle"]').click(function(event) {
  event.preventDefault();

  $.ajax({
    type: "POST",
    url: "some.php",
    data: { title: $('input[name="newStyleTitle"]').val(), description: $('textarea[name="newStyleDescription"]').val, photo: "this photo id" }
  })
  .done(function( msg ) {
    alert( "Data Saved: " + msg );
  });

  $('.newStyle').fadeOut();
  $(".blackOverlay").fadeOut();
});

//CANCEL DIALOG

$('#cancelNewStyle').click(function(event) {
  event.preventDefault();
    $('.newStyle').fadeOut();
    $(".blackOverlay").fadeOut();
});


//==========================================================================
////////////////////////////////////////////////////////////////////////////
//==========================================================================
// LOOKS
//==========================================================================
////////////////////////////////////////////////////////////////////////////
//==========================================================================

//SHOW LOOK CREATOR

  $('#showLookCreator').click(function(event) {
    if ( $(".photo .tagStyle").is(":visible") ) {
      $('.photo .tagStyle').slideUp();
      $('.lookCreator').slideToggle();
    }else {
      $('.lookCreator').slideToggle();
    }
  });

//HIDE LOOK CREATOR

  $('#hideLookCreator').click(function(event) {
    event.preventDefault();
    $('.lookCreator').slideUp(function() {
      // Animation complete.
    });
  });

//MAKE DROP BOX TARGET FOR DRAG AND DROP ACTION

  $( ".dropBox" ).droppable({
    accept: '.productThumb',
    drop: function( event, ui ) {
      
      $( this ).find( "span" ).remove();
      $(this).append($(ui.draggable).clone());
      $(".dropBox .productThumb").addClass("item");
      $(".item").removeClass("ui-draggable productThumb");

      $(".item").click(function(event) {
        event.preventDefault();
      }).draggable ({
        stack: ".dropBox .item",
        containment: ".dropBox",
        scroll: false
      }).resizable({aspectRatio: 1/1, minWidth: 65});

    }
  });

//MAKE PRODUCTS DRAGGABLE

  $(".lookProducts .productThumb").click(function(event) {
    event.preventDefault();
  }).draggable({ 
    stack: ".lookProducts .productThumb", 
    revert: "invalid", 
    scroll: false, 
    helper: 'clone'
  });

var photos = $('.photoDiv > .swipe > .swipe-wrap').children();

window.swipePhoto = new Swipe(document.getElementById('swipePhoto'), {
  startSlide: 0,
  speed: 1,
  auto: false,
  continuous: true,
  disableScroll: false,
  stopPropagation: false,
  callback: function(pos) {
     var pos = window.swipePhoto.getPos();
     var actualheight = $(photos[window.swipePhoto.getPos()]).children().css('height');

     var photoid = $(photos[pos]).children().attr("alt");
     //var photoid = $(photoselector).children().attr('data-photoid');
     //console.log(photoid);

     window.location.hash = '#'+photoid;
     $('#swipePhoto').animate({height: actualheight}, 100);

     console.log(pos);

     $(".stylephoto").eq(pos).show().lazyload();
  },
  transitionEnd: function(index, elem) {
     //console.log("jlaa");
     $('.tagBox').html('');
  }
});
//
// Make thumb clickables
$('.thumb').children().each(function(){
     var photoid = $(this).attr("data-photoid");
     var position = parseInt($('.lazy[data-photoid="'+photoid+'"]').parent().attr('data-index'));

     $(this).bind("click", function(){
        window.swipePhoto.slide(position, 0);
        //$('html, body').animate({scrollTop: $("#tabs").offset().top}, 1000); //scroll top to photo
     });
});



$(document).keydown(function(e){
    if (e.keyCode == 37) { 
       swipePhoto.prev();
       return false;
    }else if (e.keyCode == 39){
       swipePhoto.next();
       return false;
    }
    
    // else if (e.keyCode == 76){
    //    $('button.love').click();
    //    return false;
    // }else if (e.keyCode == 84){
    //    $('button#showTagCreator').click();
    //    return false;
    // }else if (e.keyCode == 67){
    //    $('button#showLookCreator').click();
    //    return false;
    // }

});


//$('img.cachedphoto').load(function(){
//      console.log("hola cargado");
//});

//$('.cachedphoto').bind("load", function(){
//     var swipephotoheight = $(".cachedphoto").first().css("height");
//     $('#swipePhoto').css("height", swipephotoheight  );     
//     window.location.hash = '#'+$('.cachedphoto').first().attr('data-photoid');
//});

// lazy photo loads

//$(".stylephoto").slice(0,5).show().lazyload();

//var pos = window.swipePhoto.getPos();
//$('.slidephoto').eq(0).show().lazyload(); //some event is not loaded yet and this doesnt work here, gonna make it in bakcend mode
//$('.slidephoto').lazyload();


//  $(photos[0]).bind('load',function(){
//      console.log("foto cargada");
//      var actualheight = $('[data-index="0"]').css('height');
//      $('#swipePhoto').css('height', actualheight);
//  });


  //==========================================================================
////////////////////////////////////////////////////////////////////////////
//==========================================================================
// USER PROFILE
//==========================================================================
////////////////////////////////////////////////////////////////////////////
//==========================================================================

//==========================================================================
// EDIT SETTINGS (PASSWORD, EMAIL, ETC:)
//==========================================================================

$("#userSettings").click(function(event) {

  event.preventDefault();

  //SHOW DIALOG FOR SETTINGS
  $(".blackOverlay").fadeIn();
  timeoutID = window.setTimeout(showSettings, 800);
  function showSettings() {
    $('.userSettings').fadeIn(800);
    //FOCUS FIRST INPUT
    $("#userFirstName").focus();
  }

});

//UPDATE THE SETTINGS (AJAX) AND FADE OUT DIALOG

$('.userSettings input[name="updateUserSettings"]').click(function(event) {
  event.preventDefault();

  $.ajax({
    type: "POST",
    url: "some.php",
    data: { 
      firstName: $('input[name="First Name"]').val(),
      lastName: $('input[name="Last Name"]').val,
      email: $('input[name="Email"]').val,
      userName: $('input[name="Username"]').val,
      password: $('input[name="Password"]').val 
    }
  })
  .done(function( msg ) {
    alert( "Data Saved: " + msg );
  });

  $('.userSettings').fadeOut();
  $(".blackOverlay").fadeOut();
});

//CANCEL DIALOG

$('#cancelUserSettings').click(function(event) {
  event.preventDefault();
    $('.userSettings').fadeOut();
    $(".blackOverlay").fadeOut();
});

//==========================================================================
// EDIT USER INFO
//==========================================================================

var userInfo = $("#styleText").text();
var editButton = "";

//COPY TEXT TO EDITABLE TEXTAREA

$(".userBox").on("click", "#editInfo", function(event) {

  event.preventDefault();

  var doIt = editInfo(".userBox", $(this).attr("id"));

  //SAVE EDITED TEXT

  $("#saveInfo").click(function(event) {

    event.preventDefault();

    var newUserInfo = $(".editInfo").val();

    $(".editInfo").replaceWith($("<p id='styleText'>" + newUserInfo + "</p>"));
    $(this).parent().append(editButton);
    $(this).remove();
    $("#cancelInfo").remove();
    $('#styleText').readmore({
      speed: 250,
      maxHeight: 42,
      moreLink: '<a href="#" class="readMore inline">Read more</a>',
      lessLink: '<a href="#" class="readMore inline">Hide</a>'
    });

    $.ajax({
      type: "POST",
      url: "some.php",
      data: { 
        user_id: "this user id",
        user_info: newUserInfo
      }
    })
    .done(function( msg ) {
      alert( "Data Saved: " + msg );
    });

  });

  //CANCEL EDITING MODE

  $("#cancelInfo").click(function(event) {

    event.preventDefault();

    $(".editInfo").replaceWith($("<p id='styleText'>" + userInfo + "</p>"));
    $(this).parent().append(editButton);
    $(this).remove();
    $("#saveInfo").remove();
    $('#styleText').readmore({
      speed: 250,
      maxHeight: 42,
      moreLink: '<a href="#" class="readMore inline">Read more</a>',
      lessLink: '<a href="#" class="readMore inline">Hide</a>'
    });

  });

});

$(".galleryInfo").on("click", "#editInfo", function(event) {

  event.preventDefault();

  var doIt = editInfo(".galleryInfo", $(this).attr("id"));

  //SAVE EDITED TEXT

  $("#saveInfo").click(function(event) {

    event.preventDefault();

    var newGalleryInfo = $(".editInfo").val();

    $(".editInfo").replaceWith($("<p id='styleText'>" + newGalleryInfo + "</p>"));
    $(this).parent().append(editButton);
    $(this).remove();
    $("#cancelInfo").remove();
    $('#styleText').readmore({
      speed: 250,
      maxHeight: 42,
      moreLink: '<a href="#" class="readMore inline">Read more</a>',
      lessLink: '<a href="#" class="readMore inline">Hide</a>'
    });

    $.ajax({
      type: "POST",
      url: "some.php",
      data: { 
        style_id: "this style id",
        user_info: newGalleryInfo
      }
    })
    .done(function( msg ) {
      alert( "Data Saved: " + msg );
    });

  });

  //CANCEL EDITING MODE

  $("#cancelInfo").click(function(event) {

    event.preventDefault();

    $(".editInfo").replaceWith($("<p id='styleText'>" + userInfo + "</p>"));
    $(this).parent().append(editButton);
    $(this).remove();
    $("#saveInfo").remove();
    $('#styleText').readmore({
      speed: 250,
      maxHeight: 42,
      moreLink: '<a href="#" class="readMore inline">Read more</a>',
      lessLink: '<a href="#" class="readMore inline">Hide</a>'
    });

  });

});

function editInfo(context, clicked) {

  editButton = $("#" + clicked).get(0);

  $(context + " .readmore-js-toggle").remove();
  $("#styleText").replaceWith($("<textarea class='editInfo'>" + userInfo + "</textarea>"));
  $("#" + clicked).replaceWith("<a href='#' id='saveInfo' class='button'>Save</a>");
  $(context).append("<a href='#' id='cancelInfo' class='button'>Cancel</a>")
  $(".editInfo").focus();

}


//==========================================================================
// HOME PAGE BANNER (SWIPE)
//==========================================================================

if( $('#slider').length > 0){

    window.mySwipe = new Swipe(document.getElementById('slider'), {
      startSlide: 0,
      speed: 1000,
      auto: 4000,
      continuous: true,
      disableScroll: false,
      stopPropagation: false,
      callback: function(pos) {
    	  var i = bullets.length;
          while (i--) {
            bullets[i].className = '';
          }
          bullets[pos].className = 'on';
    	  },
      transitionEnd: function(index, elem) {}
    });
    var bullets = document.getElementById('position').getElementsByTagName('li');
}

//==========================================================================
// ADD STYLE TO STYLE FEED
//==========================================================================

$("#addStyleFeed").click(function(event) {

  event.preventDefault();

  //SHOW DIALOG FOR ADDING STYLE FEED
  $(".blackOverlay").fadeIn();
  timeoutID = window.setTimeout(showSettings, 800);
  function showSettings() {
    $('.addStyleFeed').fadeIn(800);
  }

  //CANCEL DIALOG

  $('#canceladdStyleFeed').click(function(event) {
    event.preventDefault();
      $('.addStyleFeed').fadeOut();
      $(".blackOverlay").fadeOut();
  });

});

$(".addStyleFeed input[name=tagStyle]").bind("change paste keyup", function() {

    var searchText = $(this).val();
    //console.log(searchText); 
    //console.log(searchText.length); 

    if( searchText.length == 0 ){
      //console.log("Search text is null");
      $('.addStyleFeed #swipeStyleFeed').parent().slideUp("slow");
    }else{
      $('.addStyleFeed #swipeStyleFeed').parent().slideDown("slow", function() {

        $.post('search.php', {searchReq : searchText}, function(data){

          $.each(data, function(index, value) {

            $(".styleBox").append(value);

          });

          var divs = $(".styleBox > div");

          for(var i = 0; i < divs.length; i+=4) {
            divs.slice(i, i+4).wrapAll("<div></div>");
          }

        },'json');

        window.swipeStyleFeed = new Swipe(document.getElementById('swipeStyleFeed'), {
          startSlide: 0,
          speed: 1000,
          auto: false,
          continuous: true,
          disableScroll: false,
          stopPropagation: false,
          callback: function(pos) {},
          transitionEnd: function(index, elem) {}
        });

      });
    }

  });

var styleObject = "";

$(".addStyleFeed .styleBox .divLink").click(function(event) {
  event.preventDefault();
  var style_bg = $(this).parent();
  var selected = false;

  //GET STYLE ID
  style_id = $(this).attr("id");

  if($(style_bg).hasClass("selected")) {
    $(style_bg).removeClass("selected");
    selected = false;
  }else {
    $(".addStyleFeed .styleBox div").children(".inline").removeClass("selected");
    $(style_bg).addClass("selected");
    selected = true;
    styleObject = "<div class='inline myStyleFeed styleid'><a href='#' class='removeStyleFeed'>x</a><a href='LINK TO STYLE 1' class='divLink'>Style 1</a><!--place name of style/user inside tag--><div class='imgContainer inline'><img src='http://placehold.it/110x110' alt='' width='110px' height='110px'/></div><div class='inline styleFeedInfo'><span><strong>My Style 6</strong></span><br/><span>76K <i class='icon-heart'></i></span><br/><span>by User</span></div></div>";

  }

  if(!selected){
    //yada
  }

});

//UPDATE THE SETTINGS (AJAX) AND FADE OUT DIALOG

$('.addStyleFeed input[name="updateUserSettings"]').click(function(event) {
  event.preventDefault();


  $(styleObject).insertBefore("#addStyleFeed");

  $.ajax({
    type: "POST",
    url: "some.php",
    data: { 
      user_id: $('input[name="First Name"]').val(),
      style_id: $('input[name="Last Name"]').val()
    }
  })
  .done(function( msg ) {
    alert( "Data Saved: " + msg );
  });

  $('.addStyleFeed').fadeOut();
  $(".blackOverlay").fadeOut();
});

//REMOVE FEED FROM HOME PAGE

$(".styleFeed").on("click", ".removeStyleFeed", function(event) {
  event.preventDefault();
  $(this).parent().fadeOut();

  $.ajax({
    type: "POST",
    url: "some.php",
    data: { 
      user_id: "user id",
      style_id: $(this).parent().prop("class").split(' ')[3]
    }
  })
  .done(function( msg ) {
    alert( "Data Saved: " + msg );
  });

});

});
