// Avoid `console` errors in browsers that lack a console.
(function() {
    var method;
    var noop = function () {};
    var methods = [
        'assert', 'clear', 'count', 'debug', 'dir', 'dirxml', 'error',
        'exception', 'group', 'groupCollapsed', 'groupEnd', 'info', 'log',
        'markTimeline', 'profile', 'profileEnd', 'table', 'time', 'timeEnd',
        'timeStamp', 'trace', 'warn'
    ];
    var length = methods.length;
    var console = (window.console = window.console || {});

    while (length--) {
        method = methods[length];

        // Only stub undefined methods.
        if (!console[method]) {
            console[method] = noop;
        }
    }
}());

// Place any jQuery/helper plugins in here.



$('#styleText').readmore({
    speed: 250,
    maxHeight: 42,
    moreLink: '<a href="#" class="readMore inline">Read more</a>',
    lessLink: '<a href="#" class="readMore inline">Hide</a>'
});

$('.tags').readmore({
    speed: 250,
    maxHeight: 42,
    moreLink: '<a href="#" class="readMore">Show more</a>',
    lessLink: '<a href="#" class="readMore">Hide</a>'
});


$("#loginForm").validate();
$("#signUpForm").validate();