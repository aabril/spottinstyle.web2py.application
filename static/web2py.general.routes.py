#!/usr/bin/python
# -*- coding: utf-8 -*-
# routers = dict(
#           BASE  = dict(
#                     domains = {
#                         'localhost:8000'  : 'style',
#                         'localhost'  : 'style',
#                         'lostpet.in' : 'lostpet',
#                                         }
#                       ),
#           )

routes_in = (
	('/', '/style/default/index'),

	('/panel', '/style/panel/index'),
    ('/panel/$anything', '/style/panel/$anything'),

	('/ajax', '/style/ajax/index'),
    ('/ajax/$anything', '/style/ajax/$anything'),

    ('/admin', '/admin/default/index'),
    ('/admin/$anything', '/admin/$anything'),

	('/$profile/$style', '/style/style/index/$profile/$style'),

	('/$profile', '/style/profile/index/$profile'),



    # do not reroute admin unless you want to disable it

    # # do not reroute appadmin unless you want to disable it
    # (BASE + '/$app/appadmin', '/$app/appadmin/index'),
    # (BASE + '/$app/appadmin/$anything', '/$app/appadmin/$anything'),
    # # do not reroute static files
    ('/$app/static/$anything', '/$app/static/$anything'),
    # # reroute favicon and robots, use exable for lack of better choice
    ('/favicon.ico', '/examples/static/favicon.ico'),
    ('/robots.txt', '/examples/static/robots.txt'),

    # # do other stuff
    # ((r'.*http://otherdomain.com.* (?P<any>.*)', r'/app/ctr\g<any>')),
    # # remove the BASE prefix
    # (BASE + '/$anything', '/$anything'),
)

# routes_out, like routes_in translates URL paths created with the web2py URL()
# function in the same manner that route_in translates inbound URL paths.
#

routes_out = (
    ('/style/panel','/panel'),
    ('/style/panel/$anything','/panel/$anything'),
)




# routes_out = (
#     # do not reroute admin unless you want to disable it
#     ('/admin/$anything', BASE + '/admin/$anything'),
#     # do not reroute appadmin unless you want to disable it
#     ('/$app/appadmin/$anything', BASE + '/$app/appadmin/$anything'),
#     # do not reroute static files
#     ('/$app/static/$anything', BASE + '/$app/static/$anything'),
#     # do other stuff
#     (r'.*http://otherdomain.com.* /app/ctr(?P<any>.*)', r'\g<any>'),
#     (r'/app(?P<any>.*)', r'\g<any>'),
#     # restore the BASE prefix
#     ('/$anything', BASE + '/$anything'),
# )