var ActualPhoto, StylePhotos;

//var ajaxUrl = "http://style.bressol.org";
var ajaxUrl = jQuery(location).attr('origin');


/* -- Models -- */

StylePhotos = (function() {

  function StylePhotos(name) {
    this.name = name;
  }

  StylePhotos.prototype.get = function() {
    var ajax, results;
    ajax = jQuery.ajax({
      type: "POST",
      url: ajaxUrl + "/ajax/",
      data: {style: jQuery('#stylename').attr('name')},
      dataType: "JSON",
      async: false
    });
    //console.log(jQuery.parseJSON(ajax.responseText));
    return results = jQuery.parseJSON(ajax.responseText);
  };

  return StylePhotos;

})();

ActualPhoto = (function() {

  function ActualPhoto(id) {
    this.id = id;

  }

  // ActualPhoto.prototype.loadphoto = function() {
  // }


  ActualPhoto.prototype.tags = function(id) {
    var ajax, results;
    ajax = jQuery.ajax({
      type: "POST",
      url: ajaxUrl + "/ajax/tags",
      data: {
        id: id,
      },
      dataType: "JSON",
      async: false
    });
    return results = jQuery.parseJSON(ajax.responseText);
  };


  ActualPhoto.prototype.loves = function(id) {
    var ajax, results;
    ajax = jQuery.ajax({
      type: "POST",
      url: ajaxUrl + "/ajax/loves",
      data: {
        photoid: id,
      },
      dataType: "JSON",
      async: false
    });
    return results = jQuery.parseJSON(ajax.responseText);
  };

  return ActualPhoto;

})();


// Nano Templates (Tomasz Mazur, Jacek Becela) 
// https://github.com/trix/nano
function nano(template, data) {
  return template.replace(/\{([\w\.]*)\}/g, function(str, key) {
    var keys = key.split("."), v = data[keys.shift()];
    for (var i = 0, l = keys.length; i < l; i++) v = v[keys[i]];
    return (typeof v !== "undefined" && v !== null) ? v : "";
  });
}


var preLoadImages = function(imageList, callback) {
    var count = 0;
    var loadNext = function(){
        var pic = new Image();
        pic.onload = function(){
            count++;
            if(count >= imageList.length){
                callback();
            }
            else{
                loadNext();
            }
        }
        pic.src = imageList[count];
    }
    loadNext();
};





/* -- Controller -- */

jQuery(document).ready(function(){
  var actualphoto, photos;

  var stylephotos = new StylePhotos;
  var photoindex = 0;
  var photos = stylephotos.get();


    var photolist = new Array();

    for (var i=0,len=photos.length; i<len; i++)
    { 
        console.log(photos[i]);
        console.log(photos[i]['filename']);
        //photos[i]['img'] = 'test';
        photos[i]['img']= (new Image).src = ajaxUrl+'/style/default/download/'+photos[i]['filename'];
        photolist.push(ajaxUrl+'/style/default/download/'+photos[i]['filename']);

    }


    console.log("-=-=-=-=-=-=-=-=-=-=-=");
    console.log(photolist);
    console.log("-=-=-=-=-=-=-=-=-=-=-=");
    preLoadImages(photolist, function(){ alert("Done!"); });


    var files = ['file1,jpg', 'file2.jpg'];
    preLoadImages(files, function(){ alert("Done!"); });







 
  render();


  function render(){


    var photodisplay = photos[photoindex];

    console.log(photodisplay);

    var photourl = photos[photoindex]['filename'];
    console.log(photourl);

    var photoid = photos[photoindex]['id'];

    var actualphoto = new ActualPhoto;
    var tags = actualphoto.tags(photoid);

    // Render Loves
    var loves = actualphoto.loves(photoid);
    jQuery('#loves > span').text(loves.length);

    // Render photo owner
    jQuery('#photoownedby > span').text(photos[photoindex]['owner']);


    // count position
    jQuery('#count > span#position').text(photoindex+1);
    jQuery('#count > span#total').text(photos.length);

    // render photos
    jQuery('#main_photo').attr('src', ajaxUrl+'/style/default/download/'+photourl);
    
    //render tags
    jQuery('#tags').empty();
    for (var i=0; i<tags.length; i++){
      //console.log(tags[i]);
      var tag = nano('<div class="tag"><a href="'+ajaxUrl+'/{owner}/{name}">{name}</a></div>', tags[i]);
      jQuery('#tags').append(tag);
    };







    for (var i=0,len=photos.length; i<len; i++)
    { 
        console.log(photos[i]);
    }


  }



  jQuery('#photo').click(function(){
    //alert("hey, you clicked");

    //next
    if(!(photos.length-1==photoindex)){
       photoindex = photoindex + 1;
    }else{
       photoindex = 0;
    }

    render();





    //render thumbs
    //jQuery('#thumbs').html("asdfasdfasfdasad");

  });



  //jQuery('#tags').html(template(actualphoto.tags()));

});


