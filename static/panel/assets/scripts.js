$(function() {
    // Side Bar Toggle
    $('.hide-sidebar').click(function() {
	  $('#sidebar').hide('fast', function() {
	  	$('#content').removeClass('span9');
	  	$('#content').addClass('span12');
	  	$('.hide-sidebar').hide();
	  	$('.show-sidebar').show();
	  });
	});

	$('.show-sidebar').click(function() {
		$('#content').removeClass('span12');
	   	$('#content').addClass('span9');
	   	$('.show-sidebar').hide();
	   	$('.hide-sidebar').show();
	  	$('#sidebar').show('fast');
	});

	// Following Mouse
	$('.following').mouseover(function(){ 
	   $(this).removeClass('btn-success');
	   $(this).addClass('btn-danger');
	   $(this).text('Unfollow');
	}).mouseout(function(){
	   $(this).removeClass('btn-danger');
	   $(this).addClass('btn-success');
	   $(this).text('Following');
	});

	// Love Mouse
	$('.loved').mouseover(function(){ 
	   $(this).removeClass('btn-success');
	   $(this).addClass('btn-danger');
	   // <i class="icon-heart" style="color: white;"></i> 
	   $(this).html('<i class="icon-heart" style="color: white;"></i> UnLove');
	}).mouseout(function(){
	   $(this).removeClass('btn-danger');
	   $(this).addClass('btn-success');
	   // $(this).add('<i class="icon-heart" style="color: white;"></i>');
	   // <i class="icon-heart" style="color: white;"></i> 
	   $(this).html('<i class="icon-heart" style="color: white;"></i> Loved');
	});

	$('.hover').mouseover(function() {
   	   $(this).css('background-color', '#08c');
  	}).mouseout(function(){
       $(this).css('background-color', '#333');
 	});

	$('.hover > .deletestyle').mouseover(function(event) {
		event.stopPropagation();
   	   $(this).parent().css('background-color', 'red');
  	}).mouseout(function(){
  		event.stopPropagation();
        $(this).parent().css('background-color', '#333');
 	});




	// Following Click
	var urlunfollow = "http://"+$(location).attr('host')+"/panel/_unfollow";
	var urlfollow = "http://"+$(location).attr('host')+"/panel/_follow";
    var urldeletestyle = "http://"+$(location).attr('host')+"/panel/_deletestyle";
    var success = function(){alert("Hola");};
    var dataType = "json";

    $('.deletestyle').click(function(){
        var data = {styleid: $(this).attr('id')};
        console.log("blbllallaas");
        console.log(data);

		$.ajax({
		  type: "POST",
		  url: urldeletestyle,
		  data: data,
		  success: success,
		  dataType: dataType
		});
    })


	$('.following').click(function(){
        var data = {following: $(this).parent().attr('following'), follower: $(this).parent().attr('follower'), type: $(this).parent().attr('type')};
		$.ajax({
		  type: "POST",
		  url: urlunfollow,
		  data: data,
		  success: success,
		  dataType: dataType
		});
	});

	$('.follow').click(function(){
        var data = {following: $(this).parent().attr('following'), follower: $(this).parent().attr('follower'), type: $(this).parent().attr('type')};
		$.ajax({
		  type: "POST",
		  url: urlfollow,
		  data: data,
		  success: success,
		  dataType: dataType
		});
	});


    // Loves Click
	var urllove = "http://"+$(location).attr('host')+"/panel/_love";
	var urlunlove = "http://"+$(location).attr('host')+"/panel/_unlove";
    var success = function(msg){console.log(msg);};
    var dataType = "json";

	$('.love').click(function(){
        var data = {love: $(this).parent().attr('love'), user: $(this).parent().attr('user')};
		$.ajax({
		  type: "POST",
		  url: urllove,
		  data: data,
		  success: success,
		  dataType: dataType
		});
	});

	$('.loved').click(function(){
        var data = {love: $(this).parent().attr('love'), user: $(this).parent().attr('user')};
		$.ajax({
		  type: "POST",
		  url: urlunlove,
		  data: data,
		  success: success,
		  dataType: dataType
		});
	});


	//Tag a Style Behaviour (avoid dissapear on click)
	$('.dropdown-menu > li > input').click( function(e){
   		 e.stopPropagation();
   		 //$('.tagstyle > .dropdown-menu > li > input').focus();
	});


	var urlstylesearch = "http://"+$(location).attr('host')+"/panel/_stylesearch";
	var urlstylecreate= "http://"+$(location).attr('host')+"/panel/_stylecreate";
	var urltagstyle     ="http://"+$(location).attr('host')+"/panel/_tagstyle/"; //urlstyleid+msg[i].id
    var success = function(msg){
    	console.log(msg);
    	$('.searchresult').remove();
    	for (var i=0;i<msg.length;i++)
		{ 
           var html = '<li class="searchresult" id="'+msg[i].id+'"><a href="'+$(location)[0].href+'"><span class="label label-inverse">'+msg[i].name+'</span><span class="label label-warning">'+msg[i].description+' loves</span></a></li>';
		   $('.stylesearch').parent().after(html);
		}

		$('.searchresult').unbind();
	    $('.searchresult').click(function(){ 

	        var data = { styleid: $(this).attr('id'), photoid: $(location)[0].pathname.substr($(location)[0].pathname.lastIndexOf('/') + 1) };
	        // var data = {}
	        console.log($(this).attr('id'));
	        // console.log("inside click item");

			$.ajax({
				type: "POST",
				url: urltagstyle,
				data: data,
				success: success,
				dataType: dataType
			});    
		});

		$('.createstyle').unbind();
	    $('.createstyle').click(function(){ 

	        var data = { newstyle: $('.stylesearch').val(), photoid: $(this).closest("li").attr('id') };
	        // var data = {}
	        // console.log($('.stylesearch').val());
	        // console.log("inside click item");

			$.ajax({
				type: "POST",
				url: urlstylecreate,
				data: data,
				success: success,
				dataType: dataType
			});    
		});




    };

	//AJAX style search 
	$('.stylesearch').keyup( function(e){
         console.log($(this).val());

         var data = {keyword: $(this).val() };

         if($(this).val() != ""){
	         $.ajax({
			  type: "POST",
			  url: urlstylesearch,
			  data: data,
			  success: success,
			  dataType: dataType
			});

            $('.createstyle').html('<a href=""><h6>Create Style <span class="label label-inverse">'+$(this).val()+'</span></h6></a>');
            $('.createstyle').css('margin-top', '12px');
         }else{
    		$('.searchresult').remove();
    		$('.createstyle').html('');
         };
	});

    //panel/photo arrows to navigate between photos
	$(document).keydown(function(event) {
		if($(location).attr('pathname').indexOf("/photo/") !== -1 ){
			switch (event.keyCode) {
				case 37: $(location).attr('href', $(location).attr('origin')+$('.icon-circle-arrow-left').parent().attr('href') ); break;
				case 39: $(location).attr('href', $(location).attr('origin')+$('.icon-circle-arrow-right').parent().attr('href') ); break;
		}
    }

	});




});