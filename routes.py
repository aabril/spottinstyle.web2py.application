# -*- coding: utf-8 -*-

#  This is an app-specific example router

routes_in = (
    ('/', '/style/default/index'),

    ('/panel', '/style/panel/index'),
    ('/panel/$anything', '/style/panel/$anything'),

    ('/photo', '/style/photo/index'),
    ('/photo/$anything', '/style/photo/index/$anything'),

    ('/ajax', '/style/ajax/index'),
    ('/ajax/$anything', '/style/ajax/$anything'),

    ('api.spottinstyle.com','/style/api/index'),
    ('api.spottinstyle.com/$anything','/style/api/$anything'),

    ('/api', '/style/api/index'),
    ('/api/$anything', '/style/api/$anything'),

    ('/login', '/style/login/index'),
    ('/login/$anything', '/style/login/$anything'),

    ('/graph', '/style/graph/index'),
    ('/graph/$anything', '/style/graph/$anything'),

    ('/admin', '/admin/default/index'),
    ('/admin/$anything', '/admin/$anything'),


    ('/user', '/style/default/user/'),
    ('/user/$anything', '/style/default/user/$anything'),

    ('/all-photos', '/style/allphotos/index'),
    ('/allphotos', '/style/allphotos/index'),

    ('/latest-looks', '/style/latestlooks/index'),
    ('/latests-looks', '/style/latestlooks/index'),
    ('/latestlooks', '/style/latestlooks/index'),

    ('/users-follow', '/style/usersfollow/index'),
    ('/usersfollow', '/style/usersfollow/index'),

    ('/trendiest', '/style/trendiest/index'),

    ('/terms-and-conditions', '/style/terms/index'),


    ('/$profile', '/style/profile/index/$profile'),
    ('/profile/$profile', '/style/profile/index/$profile'),
    #("('/profile/(?P<profile>[\w./]+)'", '/style/profile/index/$profile'),

    #('/default/download/(?P<anything>.*)', '/default/download/\g<anything>'),

    ('/$profile/$style', '/style/style/index/$profile/$style'),
    #('/$profile/(?P<style>.*)', '/style/style/index/$profile/\g<style>'),
    #('/$profile/$style/$function', '/style/style/index/$profile/$style/$function'),



    ('/$app/static/$anything', '/$app/static/$anything'),
    ('/favicon.ico', '/examples/static/favicon.ico'),
    ('/robots.txt', '/examples/static/robots.txt'),
)


routes_out = (
    ('/style/panel','/panel'),
    ('/style/default/index','/'),
    ('/style/panel/$anything','/panel/$anything'),
    ('/style/user/$username', '/user/$username'),
)

routes_onerror = [
    ('style/*', '/style/default/handle_error')
]



#NOTE! To change language in your application using these rules add this line
#in one of your models files:
#   if request.uri_language: T.force(request.uri_language)
