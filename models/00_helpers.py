## Adding two new helpers to resize images and make thumbnails
class RESIZE(object):
    def __init__(self,nx=160,ny=80,error_message='niepoprawny plik'):
        (self.nx,self.ny,self.error_message)=(nx,ny,error_message)
    def __call__(self,value):
        if isinstance(value, str) and len(value)==0:
            return (value,None)
        from PIL import Image
        import cStringIO
        try:
            img = Image.open(value.file)
            img.thumbnail((self.nx,self.ny), Image.ANTIALIAS)
            s = cStringIO.StringIO()
            img.save(s, 'JPEG', quality=100)
            s.seek(0)
            value.file = s
        except:
            return (value, self.error_message)
        else:
            return (value, None)

def IMGTHUMB(image, basewidth=300, sizex=65, sizey=45):
    from PIL import Image, ImageOps
    import os
    size = sizex, sizey
    img = Image.open(request.folder + 'uploads/' + image)
    wpercent = (basewidth/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    img = img.resize((basewidth,hsize), Image.ANTIALIAS)
    img = ImageOps.fit(img, size, Image.ANTIALIAS)
    root,ext = os.path.splitext(image)
    thumb='%s_photothumb%s' %(root, ext)
    img.save(request.folder + 'uploads/' + thumb)
    return thumb

def IMGSTYLETHUMB(image, basewidth=300, sizex=65, sizey=45):
    from PIL import Image, ImageOps
    import os
    size = sizex, sizey
    img = Image.open(request.folder + 'uploads/' + image)
    wpercent = (basewidth/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    img = img.resize((basewidth,hsize), Image.ANTIALIAS)
    img = ImageOps.fit(img, size, Image.ANTIALIAS)
    root,ext = os.path.splitext(image)
    thumb='%s_stylethumb%s' %(root, ext)
    img.save(request.folder + 'uploads/' + thumb)
    return thumb


def IMGPREVIEW(image, basewidth=160):
    from PIL import Image
    import os
    img = Image.open(request.folder + 'uploads/' + image)
    wpercent = (basewidth/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    img = img.resize((basewidth,hsize), Image.ANTIALIAS)
    root,ext = os.path.splitext(image)
    thumb='%s_preview%s' %(root, ext)
    img.save(request.folder + 'uploads/' + thumb)
    return thumb

def IMGPICTURE(image, basewidth=700):
    from PIL import Image
    import os
    img = Image.open(request.folder + 'uploads/' + image)
    wpercent = (basewidth/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    img = img.resize((basewidth,hsize), Image.ANTIALIAS)
    root,ext = os.path.splitext(image)
    thumb='%s_picture%s' %(root, ext)
    img.save(request.folder + 'uploads/' + thumb)
    return thumb

def GETIMGDIMENSIONS(image):
    from PIL import Image
    import os
    img = Image.open(request.folder + 'uploads/' + image)
    #print img.size
    return img.size

def SIZEDHEIGHT(originalwidth, originalheight, basewidth=580):
    if originalwidth==originalheight:
        resizedheight = basewidth
    elif originalwidth < originalheight:
        resizedheight=((int(originalheight)*int(basewidth))/int(originalwidth))
    elif originalwidth > originalheight:
        resizedheight=((int(basewidth)*int(originalheight))/int(originalwidth))

    return resizedheight

