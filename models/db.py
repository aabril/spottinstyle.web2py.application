# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()

if not request.env.web2py_runtime_gae:
    ## if NOT running on Google App Engine use SQLite or other DB
    if request.env.http_host == 'beta.spottinstyle.com':
        db = DAL("mysql://spottinstyle:st7l3@localhost/style")
    else:
        db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])
    dbmongo = DAL("mongodb://spottinstyle:goydkbe7@localhost/style")
else:
    ## connect to Google BigTable (optional 'google:datastore://namespace')
    db = DAL('google:datastore')
    ## store sessions and tickets there
    session.connect(request, response, db=db)
    ## or store session in Memcache, Redis, etc.
    # from gluon.contrib.memdb import MEMDB
    ## from google.appengine.api.memcache import Client
    ## session.connect(request, response, db = MEMDB(Client()))

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files

response.optimize_css = 'concat,minify,inline'
response.optimize_js  = 'concat,minify,inline'

#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

#########################################################################
## ToDo:
## I gonna need to use some queuing system, as celery or python-rq
## Upload photos, mailing, love and tag system
## http://python-rq.org/
#########################################################################

###############
## hashids
###############
import hashids
hashid = hashids.Hashids(salt="Emma_W4tson", min_length=8)

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
crud, service, plugins = Crud(db), Service(), PluginManager()


## after auth = Auth(db)
auth.settings.extra_fields['auth_user']= [
  Field('avatar', 'upload', default="", requires = IS_IMAGE(extensions=('png','jpg','jpeg', 'gif')), writable=False, readable=False),
  Field('facebook_id', 'string'),
  Field('bio', 'string'),
  Field('slug', 'string'),
]
## before auth.define_tables(username=True)

## create all tables needed by auth if not custom tables
auth.define_tables(username=True, signature=False)


######################################################
## DEBUG
######################################################
# print "%% ==========================================="
# print "%% URIs"
# print "%% --------"
# print "%% "+str(request.now)
# print "%% --------"
# print "%% "+str(request.env.request_uri)
# print "%% ---- user -"
# print "%% "+str(auth.user)
# print "%% --------"
# print "%% request.env.remote_addr: "+str(request.env.remote_addr)
# print "%% request.env.http_x_forwarded_for : "+str(request.env.http_x_forwarded_for)
# print "%% --------"
# print "%% application: "+request.application
# print "%% controller: "+request.controller
# print "%% function: "+request.function
# print "%% web2py_original_uri: "+request.env.web2py_original_uri
# print "%% request_uri: "+request.env.request_uri
# print "%% ==========================================="
######################################################
## END DEBUG
######################################################

## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'jimmyjazzmag@gmail.com'
mail.settings.login = 'jimmyjazzmag:taburetmagazine'



############################################################################################################################
#########   NEW STUFF
############################################################################################################################

## configure auth policy
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True
auth.settings.logout_next = URL("default")
auth.settings.table_user.username.requires = IS_NOT_IN_DB(db, auth.settings.table_user.username)
## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
#from gluon.contrib.login_methods.rpx_account import use_janrain
#use_janrain(auth,filename='private/janrain.key')

CLIENT_ID = "390228231100178"
CLIENT_SECRET = "455003725b59fe20e8f1fedc27dd03f4"



# authentication with social media
if session.auth_with:
    if session.auth_with == 'twitter':
        from applications.style.modules import twitter_account
        toa = local_import('twitter_oauth_data')
        CLIENT_ID = toa.CLIENT_ID
        CLIENT_SECRET = toa.CLIENT_SECRET
        AUTH_URL = toa.AUTH_URL
        TOKEN_URL = toa.TOKEN_URL
        ACCESS_TOKEN_URL = toa.ACCESS_TOKEN_URL

        auth.settings.login_form = twitter_account.TwitterAccount(globals(),
                                                        CLIENT_ID,
                                                        CLIENT_SECRET,
                                                        AUTH_URL, TOKEN_URL,
                                                        ACCESS_TOKEN_URL)

    if session.auth_with == 'facebook':
        # from applications.style.modules import facebook_account
        # auth.settings.login_form = facebook_account.FaceBookAccount(globals(), db)
        auth.settings.login_form = FacebookLogin(globals())

############################################################################################################################
#########   END NEW STUFF
############################################################################################################################



############################################################################################################################
#########   OLD STUFF - DELETE
############################################################################################################################

# ## configure auth policy
# auth.settings.registration_requires_verification = True
# auth.settings.registration_requires_approval = False
# auth.settings.reset_password_requires_verification = True

# ## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
# ## register with janrain.com, write your domain:api_key in private/janrain.key
# # from gluon.contrib.login_methods.rpx_account import use_janrain
# # use_janrain(auth, filename='private/janrain.key')

# crud.settings.auth = None                      # =auth to enforce authorization on crud
# auth.settings.actions_disabled=['register','change_password','request_reset_password','profile']
# session.auth_with = None

# print "************"
# print request.now
# print session

# print session.auth_with

# if session.auth_with:
#     if session.auth_with == 'facebook':
#         print "£££££££ inside if session.auth_with == 'facebook'"
#         auth.settings.login_form = FacebookLogin(globals())
#         session.auth_with = 'facebook'
#         print "£££££££ inside if session.auth_with == 'facebook'"

# print "************"

# auth.settings.login_form=FacebookLogin(globals())
# #auth.settings.login_form = ExtendedLoginForm(auth, FacebookLogin(globals()) , signals=['token'])
############################################################################################################################
#########   END OLD STUFF - DELETE
############################################################################################################################


#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

## enabling username for auth.user table
# auth.define_tables(username=True)

db.define_table("slides",
    Field("picture", 'upload', requires = IS_IMAGE(extensions=('png','jpg','jpeg'),  minsize=(980, 420), maxsize=(980, 420)) ),
    # Field("picture", 'upload', requires = IS_IMAGE(extensions=('png','jpg','jpeg'),  minsize=(50, 40), maxsize=(50, 40)) ),
    Field('preview', 'upload', compute=lambda r: IMGPREVIEW(r['picture'], 245)),
    Field('thumb', 'upload', compute=lambda r: IMGPREVIEW(r['picture'], 245)),
    Field("name", "string"),
    Field("linkurl", "string", default=""),
    Field("sortorder", 'integer', unique=True ),
    Field("enabled", 'boolean', default=False)
)

# galleries: id, name, owner, products, loves
# db.define_table("galleries",
#     Field("name", "string"),
#     Field('owner_id', db.auth_user, default=auth.user_id, readable=False,  writable=False),
#     Field("description","text"),
# )

# galleries: id, name, owner, products, loves
db.define_table("styles",
    Field("name", "string"),
    Field('owner_id', db.auth_user, default=auth.user_id, readable=False,  writable=False),
    Field("description","text", default=""),
    Field("preview","upload", requires = IS_IMAGE(extensions=('png','jpg','jpeg','JPG')) ),
    Field('stylethumb', 'upload', compute=lambda r: IMGSTYLETHUMB(r['filename'], 300, 110, 110)),
    Field("created", "datetime", default=request.now),
)

# photos: id, datetime, location, tags, loves, comments
db.define_table("photos",
    Field('owner_id', db.auth_user, default=auth.user_id, readable=False,  writable=False),
    Field("datetime", "datetime", default=request.now, readable=False, writable=False),
    Field("filename", 'upload', requires = IS_IMAGE(extensions=('png','jpg','jpeg','JPG')) ),
    Field("picture", 'upload', compute=lambda r: IMGPICTURE(r['filename'])),
    Field('preview', 'upload', compute=lambda r: IMGPREVIEW(r['filename'])), #todo: carefully remove this
    Field('thumb', 'upload', compute=lambda r: IMGTHUMB(r['filename'])),
    Field('height', 'integer', compute=lambda r: GETIMGDIMENSIONS(r['filename'])[1]),
    Field('width', 'integer', compute=lambda r: GETIMGDIMENSIONS(r['filename'])[0]),
    Field('frommobile', 'boolean', default=False),
    #Field("styles", "list:reference styles"),
    #Field("tags", 'reference: photo_styles'),
)

# photos belongs to many styles
db.define_table('photo_tags',
    Field('author', db.auth_user),
    Field('photos', 'reference photos'),
    Field('styles', 'reference styles'),
    Field('created', "datetime", default=request.now, readable=False, writable=False)
)

# looks (kind of category): id, name, image, products, totalloves, created_by
db.define_table("looks",
    Field("owner_id", db.auth_user, default=auth.user_id, readable=False,  writable=False),
    Field("photo_id", 'reference photos'),
    Field("htmlcontent", "text", default=""),
    Field("loves", "integer", default=0),
    Field('created', "datetime", default=request.now, readable=False, writable=False)
)

# loves
# dbmongo.define_table("loves",
#    Field("owner_id", db.auth_user, default=auth.user_id, readable=False, writable=False),
#    Field("photo_id", db.photos, readable=False,  writable=False, required=True, requires=IS_INT_IN_RANGE(0, 1e100)),
#    Field("style_id", db.styles, readable=False,  writable=False, required=True, requires=IS_INT_IN_RANGE(0, 1e100)),
#    Field("created", "datetime", default=request.now, readable=False, writable=False)
# )

db.define_table("loves",
    Field("owner_id", db.auth_user, default=auth.user_id, readable=False, writable=False),
    Field("photo_id", db.photos, readable=False,  writable=False, required=True, requires=IS_INT_IN_RANGE(0, 1e100)),
    Field("style_id", db.styles, readable=False,  writable=False, required=True, requires=IS_INT_IN_RANGE(0, 1e100)),
    Field("created", "datetime", default=request.now, readable=False, writable=False)
)

# following
db.define_table("following_users",
    Field("follower", db.auth_user, default=auth.user_id, readable=False, writable=False),
    Field("follows", db.auth_user, default=auth.user_id, readable=False, writable=False),
    Field("created", "datetime", default=request.now, readable=False, writable=False)
)

db.define_table("following_styles",
    Field("follower", db.auth_user, default=auth.user_id, readable=False, writable=False),
    Field("follows", 'reference styles'),
    Field("created", "datetime", default=request.now, readable=False, writable=False)
)

db.define_table("specialthumbs",
    Field("allphotos","upload", requires = IS_IMAGE(extensions=('png','jpg','jpeg','JPG')) ),
    Field("latest","upload", requires = IS_IMAGE(extensions=('png','jpg','jpeg','JPG')) ),
    Field("trendiest","upload", requires = IS_IMAGE(extensions=('png','jpg','jpeg','JPG')) ),
    Field('allphotosthumb', 'upload', compute=lambda r: IMGTHUMB(r['allphotos'], 300, 110, 110)),
    Field('latestthumb', 'upload', compute=lambda r: IMGTHUMB(r['latest'], 300, 110, 110)),
    Field('trendiestthumb', 'upload', compute=lambda r: IMGTHUMB(r['trendiest'], 300, 110, 110)),
)


# comments: id, user.id, photo.id, datetime, text

## Redis cache
## http://www.web2py.com/books/default/chapter/29/13/deployment-recipes?search=cache#Caching-with-Redis
## http://www.web2py.com/books/default/chapter/29/13/deployment-recipes?search=cache#Sessions-in-Redis

# use Redis as cache
from gluon.contrib.redis_cache import RedisCache
cache.redis = RedisCache('localhost:6379',db=None, debug=True)

# store sessions on Redis
# from gluon.contrib.redis_session import RedisSession
# sessiondb = RedisSession('localhost:6379',db=0, session_expiry=False)
# session.connect(request, response, db = sessiondb)




#### global queries for right sidebar
# rightSidebar_recentStyles = db().select(db.styles.ALL, limitby=(0,3), orderby=~db.styles.id, cache=(cache.ram,3600),cacheable=True)
rightSidebar_recentStyles = db().select(db.styles.ALL, limitby=(0,3), orderby=~db.styles.id)
