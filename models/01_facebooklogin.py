CLIENT_ID     = "390228231100178"
CLIENT_SECRET = "455003725b59fe20e8f1fedc27dd03f4"

from facebook import GraphAPI, GraphAPIError
from gluon.contrib.login_methods.oauth20_account import OAuthAccount
from gluon.contrib.login_methods.extended_login_form import ExtendedLoginForm


class FacebookLogin(OAuthAccount):
    """OAuth impl for FaceBook"""
    AUTH_URL  = "https://graph.facebook.com/oauth/authorize"
    TOKEN_URL = "https://graph.facebook.com/oauth/access_token"

    def __init__(self, g):
        OAuthAccount.__init__(self, g, CLIENT_ID, CLIENT_SECRET,
                              self.AUTH_URL, self.TOKEN_URL,
                              scope='publish_actions, user_photos,friends_photos')
        self.graph = None

    def get_user(self):
        '''Returns the user using the Graph API.
        '''

        if not self.accessToken():
            return None
        
        if not self.graph:
            self.graph = GraphAPI((self.accessToken()))

        user = None
        try:
            user = self.graph.get_object("me")
        except GraphAPIError, e:
            self.session.token = None
            self.graph = None

        if user:
            # print user
            # https://graph.facebook.com/albert.abril/picture?type=large
            return dict(first_name = user['first_name'],
                        last_name = user['last_name'],
                        username = user['username'])
